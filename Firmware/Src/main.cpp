/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.hpp"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */

#include <hal_unican.hpp>
#include <hal_IIC_EEPROM.hpp>
#include <stm32f1xx_hal_i2c_fix.h>
#include "system.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan;
I2C_HandleTypeDef hi2c1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim4;
/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define Default_NODE_ID 100
#define NAME        "UCPF1" // Unican ProFET 1 Output
#define SW_VERSION  "V1.2"
#define HW_VERSION  "V3.0"
tUnican *pUNICAN;
tServoEEPROM *pServoEEPROM;
uint16_t CAN_Controlword;
uint16_t CAN_Statusword;

uint8_t EEPROM_OK;
uint8_t FPGA_Mirror[0x200];
volatile uint8_t MS_DONE;
uint32_t fDelay[10];
volatile uint8_t Statusled;
uint16_t counter_StatusLED = 0;

unsigned short Controlword_A, Controlword_B;
unsigned char  Aktuelle_Adresse;
unsigned char  Aktueller_Befehl;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
//static void MX_DMA_Init(void);
static void MX_CAN_Init(void);
static void MX_TIM2_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void Handle_Controlword(void);
void Handle_Statusword(void);
u_int8_t Receive_Bytecounter(void);

void profet_on_ch0(void); //Output 0 ON
void profet_on_ch1(void); //Output 1 ON
void profet_off_ch0(void); //Output 0 OFF
void profet_off_ch1(void); //Output 1 OFF
void profet_on_all(void); //Output 1 + 2 ON
void profet_off_all(void); //Output 1 + 2 OFF

void Handle_Processdata(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{

  /* USER CODE BEGIN 1 */
  HAL_DeInit();
  int8_t TPDO_State = 0;
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  HAL_Delay(100);
  pServoEEPROM = NULL;
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN_Init();
  MX_TIM2_Init();
  MX_I2C1_Init();
  MX_TIM4_Init();

  /* USER CODE BEGIN 2 */
  tServoEEPROM ServoEEPROM(EEpromMirror_Size);
  EEPROM_OK = ServoEEPROM.ConnectSerialEEPROM(&hi2c1);
  pServoEEPROM = &ServoEEPROM;
  tUnican UNICAN(&hcan, pServoEEPROM, &htim4);
  pUNICAN = &UNICAN;
  HAL_TIM_Base_Start_IT(&htim2);
  HAL_TIM_Base_Start(&htim4);
  /* USER CODE END 2 */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
    UNICAN.Kaltstart_request = false;
    UNICAN.Reset_Communication = true;

    while (!UNICAN.Handle_Coldstart())
    	    {
    	      if (!UNICAN.Kaltstart_request && UNICAN.Reset_Communication)
    	      {
    	        UNICAN.OpenCan(Default_NODE_ID, NAME, HW_VERSION, SW_VERSION);
    	        UNICAN.Reset_Communication = false;

    	        UNICAN.RPDO[0][1] = (UNICAN.RPDO[0][0] = (uint8_t*) &CAN_Controlword) + 1;
    	      }

    	      switch (TPDO_State)
    	      {
    	      	  case 3 :  if (UNICAN.Handle_TPDO(1,0,4,CAN_Statusword, *(((uint8_t *)&CAN_Statusword) +1),0,0,0,0,0,0))
    	    	  	  	  	TPDO_State++;

    	      	  case 0: 	TPDO_State = 1;
    	      	  break;

    	      	  default:   if (TPDO_State > 3)
    	      	            TPDO_State = 0;
    	      	          	TPDO_State++;
    	      	  break;
    	      }

    	      if (Statusled)//STM32 works
    	    	  HAL_GPIO_WritePin(Debug_LED_GPIO_Port,Debug_LED_Pin, GPIO_PIN_SET);
    	      else
    	    	  HAL_GPIO_WritePin(Debug_LED_GPIO_Port,Debug_LED_Pin, GPIO_PIN_RESET);

    	      UNICAN.Handle_Emergency();
    	      Handle_Processdata();
    	    }
  /* USER CODE END 3 */
  }
}
/**
  * @brief System Clock Configuration
  * @retval None
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* CAN init function */
static void MX_CAN_Init(void)
{

  hcan.Instance = CAN1;
  hcan.Init.Prescaler = 36;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SJW = CAN_SJW_1TQ;
  hcan.Init.BS1 = CAN_BS1_5TQ;
  hcan.Init.BS2 = CAN_BS2_2TQ;
  hcan.Init.TTCM = DISABLE;
  hcan.Init.ABOM = ENABLE;
  hcan.Init.AWUM = DISABLE;
  hcan.Init.NART = DISABLE;
  hcan.Init.RFLM = DISABLE;
  hcan.Init.TXFP = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }
}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }
    /* USER CODE BEGIN I2C1_Init 2 */

  if(__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_BUSY) == SET)
  {
     HAL_I2C_ClearBusyFlagErrata_2_14_7(&hi2c1, GPIOB, GPIO_PIN_6, GPIOB, GPIO_PIN_7, GPIO_MODE_AF_OD);
  }
  /* USER CODE END I2C1_Init 2 */
}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 20;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 3999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }
}

/* TIM4 init function */
static void MX_TIM4_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 20;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 0;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler((char * )__FILE__, __LINE__);
  }
}


/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* GPIO Ports Clock Enable */
	  __HAL_RCC_GPIOD_CLK_ENABLE();
	  __HAL_RCC_GPIOC_CLK_ENABLE();
	  __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOB_CLK_ENABLE();

	  /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(GPIOC, LED_extern_Pin |Debug_LED_Pin, GPIO_PIN_RESET);

	  /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(GPIOA, Channel1_Pin, GPIO_PIN_RESET);

	  /*Configure GPIO pins : PCPin PCPin PCPin PCPin */
	  GPIO_InitStruct.Pin = LED_extern_Pin|Debug_LED_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	  /*Configure GPIO pins : PAPin PAPin PAPin */
	  GPIO_InitStruct.Pin = Channel1_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

/* USER CODE BEGIN 4 */
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *hart)
//{
//	HAL_Delay(1000);
//}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
 	if(htim->Instance==htim2.Instance)
	{
    pUNICAN->Call_CIA_TIMERS();

    MS_DONE = true;
    if (pServoEEPROM != NULL)
      pServoEEPROM->EEPROM_TIMER_CALL_EVERY_MS();

    if(counter_StatusLED > 0)
    	counter_StatusLED--;
    else
    {
      counter_StatusLED = 300;
      Statusled = !Statusled;
    }
  }
}
void Handle_Processdata(void)
{
  uint16_t Controlword_temp;

  //asm(" sei");
  Controlword_temp = CAN_Controlword;
  //asm(" cli");

  if (Controlword_temp & 0x0001) profet_on_ch1(); else profet_off_ch1();
}

void Handle_Controlword(void)
{
  uint16_t Controlword_temp;
  uint16_t Controlword_Set_Back;

  // Controlword A bearbeiten
  Controlword_temp = Controlword_A;
  Controlword_Set_Back = 0xFFFF;
}

void Handle_Statusword(void)
{
  uint16_t vStatusword;
  vStatusword = 0;

  if (!Aktueller_Befehl) vStatusword = 0;
  else vStatusword |= BITMASK(0);		// In Prozess
  CAN_Statusword = vStatusword;
}


void profet_on_ch1()
{
	HAL_GPIO_WritePin(Channel1_GPIO_Port,Channel1_Pin, GPIO_PIN_SET);
}
void profet_off_ch1()
{
	HAL_GPIO_WritePin(Channel1_GPIO_Port,Channel1_Pin, GPIO_PIN_RESET);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
