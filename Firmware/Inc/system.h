/*----------------------------------------------------------------------------
 * Name:    system.h
 * Purpose: low level definitions
 * Note(s):
----------------------------------------------------------------------------- */ 

#ifndef __SYSTEM_H
#define __SYSTEM_H

#if defined(STM32F10X_MD) || defined(STM32F103xB)
#include <stm32f1xx.h>
#ifdef USE_HAL_DRIVER
#include <stm32f1xx_hal_gpio.h>
#else
#include <stm32f10x_gpio.h>
#endif

#elif defined(STM32F4XX) || defined(STM32F40_41xxx) || defined(STM32F415xx)
#include <stm32f4xx.h>
  #ifdef USE_HAL_DRIVER
    #include <stm32f4xx_hal_gpio.h>
    // Kompatibilitšt
     typedef enum
    {
      GPIO_Mode_IN   = 0x00, /*!< GPIO Input Mode */
      GPIO_Mode_OUT  = 0x01, /*!< GPIO Output Mode */
      GPIO_Mode_AF   = 0x02, /*!< GPIO Alternate function Mode */
      GPIO_Mode_AN   = 0x03  /*!< GPIO Analog Mode */
    } GPIOMode_TypeDef;


  #else
   #include <stm32f4xx_rcc.h>
    #include <stm32f4xx_gpio.h>
  #endif
			
#endif

//**** Constant ****
#define NIL ((void *)0)
#define false   0
#define true   !false

#define UI1 ((uint16_t) 1)
#define UL1 ((uint32_t) 1)
#define LENGTH(x)  (sizeof(x) / sizeof((x)[0]))
#define BITMASK(x) (UL1 << (x))
#define BOOLEAN_RESULT(x) ((x) != 0)



//#define Unique_ID1 *((uint32_t*)0x1FFF7A10)
//#define Unique_ID2 *((uint32_t*)0x1FFF7A14)
//#define Unique_ID3 *((uint32_t*)0x1FFF7A18)
	
typedef struct 
{
  uint32_t EulerPuffer;
} tEuler;


#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

void BootLoaderInit(uint32_t BootloaderStatus);
void CiaBootLoader(uint32_t BootloaderStatus);


uint32_t Intervall(uint32_t min, uint32_t Value, uint32_t max);
uint16_t EulerFilter(uint16_t Input, uint8_t Grad, tEuler *Buffer);
unsigned int calcCRC16r(unsigned int crc, unsigned int c, unsigned int mask);
unsigned int calcCRC8(unsigned char crc, unsigned char c, unsigned char mask);
void Enable_FPU(void);

#ifndef USE_HAL_DRIVER

void RCC_TIMPeriphClock_Cmd(TIM_TypeDef* TIMx, FunctionalState NewState);

#if defined(STM32F10X_MD)  || defined(STM32F103xB)
uint32_t RCC_APB2GPIO_Decoder(GPIO_TypeDef* GPIOx);
#elif defined(STM32F4XX) || defined(STM32F40_41xxx)
uint32_t RCC_AHB1GPIO_Decoder(GPIO_TypeDef* GPIOx);
#endif

uint32_t NVIC_IRQChannel_Decoder(USART_TypeDef* USARTx);
void GPIO_Config_ALL(GPIO_TypeDef* GPIOx, uint32_t GPIO_PinBits, GPIOMode_TypeDef GPIOMode, uint8_t GPIO_AF);
void GPIO_PinGroupAF(GPIO_TypeDef* GPIOx, uint32_t GPIO_PinBits, uint8_t GPIO_AF);

#endif
void DWT_Init(void);
uint32_t DWT_Get(void);
uint8_t DWT_Compare(int32_t tp);
void DWT_Delay(uint32_t us);
// uint32_t SYSCLK_Frequency(void);




#ifdef __cplusplus
  }
#endif /* __cplusplus */
#endif

