/*----------------------------------------------------------------------------
 * Name:    system.c
 * Purpose: low level definitions
 * Note(s):
----------------------------------------------------------------------------- */ 

#if defined(STM32F10X_MD)
#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#elif defined(STM32F4XX) || defined(STM32F40_41xxx)
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#elif defined(STM32F415xx)
#include <stm32f415xx.h>
#include <stm32f4xx.h>
#include "stm32f4xx_hal.h"
#elif defined(STM32F103xB)
#include <stm32f1xx.h>
#include "stm32f1xx_hal.h"
#endif

#include "system.h"
#include "stdlib.h"


extern uint32_t SystemCoreClock;

void(*SysMemBootJump) (void);


void BootLoaderInit(uint32_t BootloaderStatus)
{
	uint32_t SP   = *((uint32_t *) 0x1fff0000);               // Wert aus dem ROM
	SysMemBootJump=(void (*)(void)) (*((uint32_t*) 0x1fff0004));   // Zeiger aus dem ROM
  if (BootloaderStatus == 1)
	{
#ifndef USE_HAL_DRIVER
    RCC_DeInit();
#else
     HAL_DeInit();
#endif
		 SysTick->CTRL = 0;
		 SysTick->LOAD = 0;
		 SysTick->VAL = 0;
		
     __set_PRIMASK(1);          // Disable Interrups
		 __set_MSP(SP);             // Wert aus dem ROM
		 //__set_MSP(0x20001000);  // Defaultwert

     SysMemBootJump();	
     
     while(1);		
  }		
}

//uint32_t SYSCLK_Frequency(void)
//{
//  RCC_ClocksTypeDef RCC_Clocks;
//  RCC_GetClocksFreq (&RCC_Clocks);
//  return RCC_Clocks.SYSCLK_Frequency;
//}


__weak void CiaBootLoader(uint32_t BootloaderStatus)
{
	uint32_t SP   = *((uint32_t *) 0x0808C000);              
	SysMemBootJump=(void (*)(void)) (*((uint32_t*) 0x0808C004));
  if (BootloaderStatus == 1)
	{
     RCC->CIR = 0x00000000;  // Disable Interrups
     __set_PRIMASK(1);       // Disable Interrups
	   __set_MSP(SP);          // Defaultwert
		 SysMemBootJump();	
		 while(1);
  }
}

__weak uint32_t Intervall(uint32_t min, uint32_t Value, uint32_t max)
{
  if (min > Value) return min;
  if (max < Value) return max;
  return Value;
}

uint16_t EulerFilter(uint16_t Input, uint8_t Grad, tEuler *Buffer)
{
  int32_t Input_temp;
	int32_t Output_temp;
	if (Grad > 15) Grad = 15;
	Input_temp  = Input << Grad;
	Input_temp -= Buffer->EulerPuffer;
  Buffer->EulerPuffer  += Input_temp >> Grad;
	//Output_temp = Buffer->EulerPuffer >> Grad; 
	Output_temp = (Buffer->EulerPuffer >> Grad) + ((Buffer->EulerPuffer >> (Grad - 1)) & 1);
  return(Output_temp);
}

unsigned int calcCRC16r(unsigned int crc, unsigned int c, unsigned int mask)
{
  // LSB First (reverse)
	unsigned char i;
  for(i=0;i<8;i++)
  {
    if((crc ^ c) & 1) { crc=(crc>>1)^mask; }
    else crc>>=1;
    c>>=1;
  }
  return (crc);
}


unsigned int calcCRC8(unsigned char crc, unsigned char c, unsigned char mask)
{
  // MSB first
	unsigned char i;
  for(i=0;i<8;i++)
  {
    if((crc ^ c) & 0x80) { crc=(crc<<1)^mask; }
    else crc<<=1;
    c<<=1;
  }
  return (crc);
}

void Enable_FPU(void)
{
  SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));
}

#ifndef USE_HAL_DRIVER
	
#if defined(STM32F10X_MD)
uint32_t RCC_APB2GPIO_Decoder(GPIO_TypeDef* GPIOx)
{
  if (GPIOx==GPIOA) return RCC_APB2Periph_GPIOA;
  if (GPIOx==GPIOB) return RCC_APB2Periph_GPIOB;
  if (GPIOx==GPIOC) return RCC_APB2Periph_GPIOC;
  if (GPIOx==GPIOD) return RCC_APB2Periph_GPIOD;
  if (GPIOx==GPIOE) return RCC_APB2Periph_GPIOE;
  if (GPIOx==GPIOF) return RCC_APB2Periph_GPIOF;
  if (GPIOx==GPIOG) return RCC_APB2Periph_GPIOF;
}
#endif

#if defined(STM32F4XX) || defined(STM32F40_41xxx) || defined(STM32F415xx)
uint32_t RCC_AHB1GPIO_Decoder(GPIO_TypeDef* GPIOx)
{
// #define _N (((uint32_t)GPIOx) - GPIOA_BASE) / (GPIOB_BASE-GPIOA_BASE)
// return RCC_AHB1Periph_GPIOA << _N
  if (GPIOx==GPIOA) return RCC_AHB1Periph_GPIOA;
  if (GPIOx==GPIOB) return RCC_AHB1Periph_GPIOB;
  if (GPIOx==GPIOC) return RCC_AHB1Periph_GPIOC;
  if (GPIOx==GPIOD) return RCC_AHB1Periph_GPIOD;
  if (GPIOx==GPIOE) return RCC_AHB1Periph_GPIOE;
  if (GPIOx==GPIOF) return RCC_AHB1Periph_GPIOF;
  if (GPIOx==GPIOG) return RCC_AHB1Periph_GPIOG;
  if (GPIOx==GPIOH) return RCC_AHB1Periph_GPIOH;
  if (GPIOx==GPIOI) return RCC_AHB1Periph_GPIOI;
	return 0;
}
#endif


void RCC_TIMPeriphClock_Cmd(TIM_TypeDef* TIMx, FunctionalState NewState)
{
	uint32_t CMD;
	CMD = 0;

      if (TIMx==TIM1)  CMD = RCC_APB2Periph_TIM1;	
	if (TIMx==TIM8)  CMD = RCC_APB2Periph_TIM8;	
	if (TIMx==TIM9)  CMD = RCC_APB2Periph_TIM9;	
	if (TIMx==TIM10)  CMD = RCC_APB2Periph_TIM10;	
	if (TIMx==TIM11)  CMD = RCC_APB2Periph_TIM11;	

	if (CMD)  
	{
  	  RCC_APB2PeriphClockCmd(CMD, NewState);	
        return;
      }
// Teil2 lie�e sich so vereinfachen
// #define _N (((uint32_t)TIM2x) - TIM2_BASE) / (TIM3_BASE-TIM2_BASE)
// CMD = RCC_APB1Periph_TIM2 << _N
	if (TIMx==TIM2)  CMD = RCC_APB1Periph_TIM2;	
	if (TIMx==TIM3)  CMD = RCC_APB1Periph_TIM3;	
	if (TIMx==TIM4)  CMD = RCC_APB1Periph_TIM4;	
	if (TIMx==TIM5)  CMD = RCC_APB1Periph_TIM5;	
	if (TIMx==TIM6)  CMD = RCC_APB1Periph_TIM6;	
	if (TIMx==TIM7)  CMD = RCC_APB1Periph_TIM7;	
	if (TIMx==TIM12) CMD = RCC_APB1Periph_TIM12;	
	if (TIMx==TIM13) CMD = RCC_APB1Periph_TIM13;	
	if (TIMx==TIM14) CMD = RCC_APB1Periph_TIM14;	
	if (CMD) 
	{ 
	  RCC_APB1PeriphClockCmd(CMD, NewState);
	  return;
	}
}

uint32_t NVIC_IRQChannel_Decoder(USART_TypeDef* USARTx)
{
	 if (USARTx==USART1) return USART1_IRQn;
	 if (USARTx==USART2) return USART2_IRQn;
	 if (USARTx==USART3) return USART3_IRQn;
#if defined(STM32F4XX) || defined(STM32F40_41xxx)
	 if (USARTx==UART4)  return UART4_IRQn;
	 if (USARTx==UART5)  return UART5_IRQn;
	 if (USARTx==USART6) return USART6_IRQn;
#endif
	 return 0;
}	

void GPIO_Config_ALL(GPIO_TypeDef* GPIOx, uint32_t GPIO_PinBits, GPIOMode_TypeDef GPIOMode, uint8_t Option)
{
  // Sonderf�lle wie 
	GPIO_InitTypeDef GPIO_InitStructure;

#if defined(STM32F10X_MD)
	RCC_APB2PeriphClockCmd(RCC_APB2GPIO_Decoder(GPIOx), ENABLE);

  GPIO_StructInit(&GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin  = GPIO_PinBits;
  GPIO_InitStructure.GPIO_Mode  = GPIOMode;
  GPIO_InitStructure.GPIO_Speed = Option;
	
#elif defined(STM32F4XX) || defined(STM32F40_41xxx)
	/* Enable clock for GPIOx */
	RCC_AHB1PeriphClockCmd(RCC_AHB1GPIO_Decoder(GPIOx), ENABLE);
	
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin  = GPIO_PinBits;
	GPIO_InitStructure.GPIO_Mode  = GPIOMode;

	if ((GPIOMode == GPIO_Mode_AF) && ((Option == GPIO_AF_SPI1) || (Option == GPIO_AF_SPI3)))
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	else
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	
  if (GPIOMode == GPIO_Mode_OUT)
		GPIO_InitStructure.GPIO_OType = (GPIOOType_TypeDef) Option; // Example: GPIO_OType_PP;
	if ((GPIOMode == GPIO_Mode_IN) || (GPIOMode == GPIO_Mode_AIN))
    GPIO_InitStructure.GPIO_PuPd  = (GPIOPuPd_TypeDef) Option;	// Example:  GPIO_PuPd_NOPULL;
	if ((GPIOMode == GPIO_Mode_AF) && (Option == GPIO_AF_I2C1))    // AF4
  {	
      GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			// this defines the output type as open drain mode (as opposed to push pull)
      GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	}	
	GPIO_Init(GPIOx, &GPIO_InitStructure);
	
	if (GPIOMode == GPIO_Mode_AF)
	  GPIO_PinGroupAF(GPIOx, GPIO_PinBits, Option);	
#endif
}	




void GPIO_PinGroupAF(GPIO_TypeDef* GPIOx, uint32_t GPIO_PinBits, uint8_t GPIO_AF)
{
  uint8_t pinpos;	
	for (pinpos = 0x00; pinpos < 0x10; pinpos++)
	{
		if (GPIO_PinBits & (((uint32_t)0x01) << pinpos))
			GPIO_PinAFConfig(GPIOx, pinpos, GPIO_AF);  
	}
}

#endif


void DWT_Init(void) 
{
  if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk)) 
  {
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  }
}

uint32_t DWT_Get(void)
{
  return DWT->CYCCNT;
}


__inline
uint8_t DWT_Compare(int32_t tp)
{
  return (((int32_t)DWT_Get() - tp) <= 0);
}

void DWT_Delay(uint32_t us) // microseconds
{
  int32_t tp = DWT_Get() + us * (SystemCoreClock/1000000);
  while (DWT_Compare(tp));
}





