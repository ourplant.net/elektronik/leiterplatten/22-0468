/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32F1xx_HAL_I2C_FIX_H
#define __STM32F1xx_HAL_I2C_FIX_H

#include "stm32f1xx_hal.h"

#ifdef __cplusplus
 extern "C" {
#endif 

void HAL_I2C_ClearBusyFlagErrata_2_14_7(I2C_HandleTypeDef *hi2c, GPIO_TypeDef  *GPIO_SCL, uint32_t SCL_Pin, GPIO_TypeDef  *GPIO_SDA, uint32_t SDA_Pin, uint32_t Alternate);

#ifdef __cplusplus
}
#endif

#endif /* __STM32F1xx_HAL_I2C_FIX_H */

