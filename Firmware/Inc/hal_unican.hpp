/**
  ******************************************************************************
  * File Name          : unican.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  ******************************************************************************
  **/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UNICAN_HPP
#define __UNICAN_HPP

#if defined(STM32F10X_MD) || defined(STM32F103xB)
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_can.h"
#include "stm32f103xb.h"
#else
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_can.h"
//#include "flash.hpp"
#endif
#include <hal_IIC_EEPROM.hpp>


#ifndef UNICAN_VERSION
#define UNICAN_VERSION  "V703"
#endif


#ifndef CAN_FIFO_Size
#define CAN_FIFO_Size       6
#endif
#ifndef CAN_FIFO_TPDO_MAX
#define CAN_FIFO_TPDO_MAX   CAN_FIFO_Size - 2
#endif



#define Debugmode                     0


//**** EEPROM Konstanten
#define HC12_EEPROM_BASE          (uint32_t) 0x0d00
#define EEPROM_BASE               (uint32_t) 0x0000
#define EEPROM_SIZE               (uint32_t) 0x0100
#define EEADR_EEData_OK           (EEPROM_BASE + 1)
#define EEADR_CanOpen_Node_ID     (EEPROM_BASE + 3)
#define EEADR_Serial_Number       (EEPROM_BASE + 4)
#define EEADR_SERVO_0_Parameter_B (EEPROM_BASE + 8)
#define EEADR_SERVO_1_Parameter_B (EEPROM_BASE + 28)
#define EEADR_SERVO_2_Parameter_B (EEPROM_BASE + 48)
#define EEADR_SERVO_3_Parameter_B (EEPROM_BASE + 68)
#define EManReg_Area_size   10
#define EEADR_EManReg_Area        (EEPROM_BASE + 88)
#define EEADR_EManReg_1           (EEADR_EManReg_Area + 0)
#define EEADR_EManReg_2           (EEADR_EManReg_Area + 2)
#define EEADR_EManReg_3           (EEADR_EManReg_Area + 4)
#define EEADR_EManReg_4           (EEADR_EManReg_Area + 6)
#define EEADR_EManReg_5           (EEADR_EManReg_Area + 8)
#define EEADR_EManReg_6           (EEADR_EManReg_Area + 10)
#define EEADR_EManReg_7           (EEADR_EManReg_Area + 12)
#define EEADR_EManReg_8           (EEADR_EManReg_Area + 14)
#define EEADR_EManReg_9           (EEADR_EManReg_Area + 16)
#define EEADR_EManReg_10           (EEADR_EManReg_Area + 18)
#define EEADR_Betriebsstunden     (EEPROM_BASE + 108)
#define EEADR_Birthday            (EEPROM_BASE + 110)
#define EEADR_Next_free           (EEPROM_BASE + 112)


#define Error_Betriebsstunden_Size                 11
#define EEADR_Errors                (EEPROM_BASE + 208)
#define EEADR_Error_Betriebsstunden (EEPROM_BASE + 230)




//Mailbox
#define Standard_Mailbox 							0
#define IRQ_Mailbox      							1   // F�r den IRQ eine ander Mailbox als Standard verwenden
#define RXFIFO            						0


// Flash
#define FlashOperation         (uint16_t) 0x5496
#define ExtFlashOperation      (uint16_t) 0x5497

#define FlashStartAdress       (uint8_t) 0x01
#define FlashSize              (uint8_t) 0x02
#define FlashPageSize          (uint8_t) 0x03
#define FlashSelectPage        (uint8_t) 0x04
#define FlashPageCommand       (uint8_t) 0x05    // 0x5496, 05, 0x79 Run Bootloader

#define STM32_FLASH_SIZE              (uint32_t)0x00100000
#define STM32_PAGE_SIZE               (uint32_t)0x00004000

// Memory
#define MemoryOrganisation     (uint16_t) 0x5498
#define MemoryStartAdress1     (uint8_t) 0x01
#define MemorySize1            (uint8_t) 0x02
#define MemoryStartAdress2     (uint8_t) 0x03
#define MemorySize2            (uint8_t) 0x04


#define Set_HC12_Register_Byte (uint16_t) 0x5499

//**** ErrorCode
#define No_Error            (uint16_t) 0x0000
#define Generic_Error       (uint16_t) 0x1000
#define CAN_Overrun         (uint16_t) 0x8110
#define Error_Passive_Mode  (uint16_t) 0x8120
#define Life_Guard_Error    (uint16_t) 0x8130
#define Overcurrent_Output  (uint16_t) 0x2310

//**** Error Register
#define Generic             (uint8_t) 0x01
#define Overcurrent         (uint8_t) 0x02
#define Overvoltage         (uint8_t) 0x04
#define Overtemperature     (uint8_t) 0x01
#define Communication       (uint8_t) 0x10

#define CAN_FILTERMODE_IDMASK       ((uint8_t)0x00)  /*!< Identifier mask mode */
#define CAN_FILTERMODE_IDLIST       ((uint8_t)0x01)  /*!< Identifier list mode */

#define CAN_FILTERSCALE_16BIT       ((uint8_t)0x00)  /*!< Two 16-bit filters */
#define CAN_FILTERSCALE_32BIT       ((uint8_t)0x01)  /*!< One 32-bit filter  */


#define  FUNCTION_MASK                (uint32_t)0x780
#define  FUNCTION_CODE_SDO_TX         (uint32_t)0x580
#define  FUNCTION_CODE_SDO_RX         (uint32_t)0x600
#define  FUNCTION_CODE_EMERGENCY      (uint32_t)0x080
#define  FUNCTION_CODE_NMT            (uint32_t)0x000
#define  FUNCTION_CODE_SYNC           (uint32_t)0x080
#define  FUNCTION_CODE_TIME           (uint32_t)0x100
#define  FUNCTION_CODE_PDO_TX_BASE    (uint32_t)0x180
#define  FUNCTION_CODE_PDO_RX_BASE    (uint32_t)0x200
#define  FUNCTION_CODE_LIVEGUARD      (uint32_t)0x700
#define  FUNCTION_CODE_LSS            (uint32_t)0x780

#define  ID_MASK                      (uint32_t)0x07F

// SDO Control
#define CONTROL_BYTE_CCS_MASK         0xE0 // Sende Kontrollbyte client command specifier
#define INITIATE_UPLOAD_REQUEST       0x40 // Anforderung Lesezugriff normal
#define INITIATE_DOWNLOAD_REQUEST     0x20 // Anforderung Schreibzugriff normal
#define UPLOAD_SEGMENT_REQUEST        0x60 // Anforderung Lesezugriff > 4 Byte}
#define DOWNLOAD_SEGMENT_REQUEST      0x00 // Anforderung Schreibzugriff > 4 Byte}
#define SEGMENTED_TOGGLE_BIT          0x10
#define SEGMENTED_7BYTES              0x00
#define SEGMENTED_NO_MORE_SEGMENTS    0x01
#define SDO_xCS_SIZE_MASK             0x0F
#define SDO_xCS_SIZE_1BYTE            0x0F
#define SDO_xCS_SIZE_2BYTE            0x0B
#define SDO_xCS_SIZE_3BYTE            0x07
#define SDO_xCS_SIZE_4BYTE            0x03
#define SDO_xCS_SIZE_DEFINED_IN_DATA  0x01
#define SDO_xCS_SIZE_UNDEFINED        0x00
#define SDO_xCS_SIZE_CONTROL_MASK     0x03

#define CONTROL_BYTE_SCS_MASK         0xE0 // Empfangs Kontrollbyte - server command specifier
#define INITIATE_UPLOAD_RESPONCE      0x40 // Antwort Lesezugriff normal
#define INITIATE_DOWNLOAD_RESPONCE    0x60 // Antwort Schreibzugriff normal
#define UPLOAD_SEGMENT_RESPONCE       0x00 // Antwort Lesezugriff > 4 Byte}
#define DOWNLOAD_SEGMENT_RESPONCE     0x20 // Antwort Schreibzugriff > 4 Byte
#define ABORT_DOMAIN_TRANSFER         0x80 // Anzeige �bertragungsabbruch von Ger�t

// Manufacturer Specific Objects
#define Set_Serial_number   (uint16_t) 0x5430
#define Select_Device_SN    (uint16_t) 0x5431
#define Set_New_Node_ID     (uint16_t) 0x5432

#define Set_ManReg_EEPROM   (uint16_t) 0x5441

#define Set_SERVO_0_Direct  (uint16_t) 0x5450  // A-Achse
#define Set_SERVO_0_EEPROM  (uint16_t) 0x5451
#define Set_SERVO_1_Direct  (uint16_t) 0x5452  // X-Achse
#define Set_SERVO_1_EEPROM  (uint16_t) 0x5453
#define Set_SERVO_2_Direct  (uint16_t) 0x5454  // Y-Achse
#define Set_SERVO_2_EEPROM  (uint16_t) 0x5455
#define Set_SERVO_3_Direct  (uint16_t) 0x5456  // Reserve
#define Set_SERVO_3_EEPROM  (uint16_t) 0x5457

#define Get_Betriebsstunden    (uint16_t) 0x5480
#define Get_Birtday            (uint16_t) 0x5481
#define Get_FIFO_State         (uint16_t) 0x5490
#define Get_CYCLE_State        (uint16_t) 0x5491

//#define STM32_UUID ((uint32_t *) UID_BASE)//((uint32_t *)  0x1FFFF7E8)
#define Unique_ID1 *((uint32_t*)(UID_BASE+0))
#define Unique_ID2 *((uint32_t*)(UID_BASE+4))
#define Unique_ID3 *((uint32_t*)(UID_BASE+8))

#define SC_NMT_STOPPED          (uint32_t) 0x02
#define SC_Preoperational       (uint32_t) 0x08
#define SC_NODEGUARD_Toggle_Bit (uint32_t) 0x20

#define STATISTIK_TIMER ((TIM_TypeDef*) TIM3)

#define  MAX_HANDLERS 10

typedef struct
{
  uint8_t  FIFO_Filling;
  uint8_t  FIFO_Max_Filling;
}  tCAN_FIFO_STATISTIK;



typedef struct
{
  uint32_t  miliseconds;
  uint16_t days;
} tTIME;


typedef struct
{
  uint16_t CANopenReg;
  uint8_t  SubIndex;
  uint8_t  IsPointer;
  uint32_t Value;
  uint32_t  Length;
} tCANopenRegisterLUT;

struct tcan_buf   // receive hat kein Prio!
{
  unsigned long id_field;
  unsigned char data_field[8];
  unsigned char dlc;
  unsigned char prio;
  unsigned char unused[2];
};

struct tCAN_FIFO
{
  unsigned char  FIFO_in_Pointer;
  unsigned char  FIFO_out_Pointer;
  unsigned char  FIFO_Filling;
  unsigned char  FIFO_Max_Filling;
  struct  tcan_buf  can_buf[CAN_FIFO_Size];
};



uint8_t CUSTOM_Register_Write(uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size);
uint8_t CUSTOM_Register_Read(uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size);

void OnCUSTOM_Register_Read_DONE(void);
void OnCUSTOM_Register_Write_DONE(void);

/*extern*/ void SPI_CANopen_Send16(uint8_t Adress, uint16_t Data);
/*extern*/ int16_t SPI_CANopen_Receive16(uint8_t Adress);

void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* hcan);
void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan);

class tUnican {
private:

  CanTxMsgTypeDef TxMessage;
  CanRxMsgTypeDef RxMessage;
  uint8_t fNode_ID;
  uint8_t fError_register;
  uint8_t fError_register_old;
  uint32_t fStatus_register;
  uint32_t fStatus_register_old;
  char const*fHW_NAME;
  char const*fSW_VER;
  char const*fHW_VER;
  uint16_t fBL_VER;
  uint32_t fSerial_Number;
  uint16_t fControlword;
  uint16_t fStatusword;
  uint8_t fTPDO_enabled[4], fTPDO_enabled_old[4];
  uint8_t fTPDO_DATA_OLD[4][8];
  uint8_t fSend_sync_PDO[4];
  int32_t fSoftware_position_limit[8];
  uint16_t fEmergency_Error_code;
  uint16_t fEmergency_Error_code_old;
  uint8_t fDevice_select;
  uint16_t fHeartbeatTime;
  uint32_t fState_Control;
  uint32_t fRT_Timer, fCycleTime, fCycleMax, fCycleMin;
  uint16_t fRX_Errors;
  uint16_t fTX_Errors;
  uint8_t fNUL;
  uint16_t fCurrent_SDO_Register;
  uint16_t fReceiveErrorCounter_temp;
  uint32_t fCAN_Controller_Errorregister;
  uint32_t fBytes_to_Send;
  uint8_t *fPointer_for_Send;
  uint32_t fBirthday;
  uint32_t fBetriebsstunden;
  volatile uint16_t fHeartbeatTimer;
  volatile uint16_t fLivetimer;
  volatile uint16_t fLivetimer_sekunden;
  tServoEEPROM *pCANEEPROM;
  //uint32_t fFlaschMemoryAddress;
  uint8_t CAN_FIFO_ADD(struct tCAN_FIFO *FIFO, struct tcan_buf *Data, uint8_t CIDACin);
  uint8_t CAN_FIFO_GET(struct tCAN_FIFO *FIFO,struct tcan_buf *Data, uint8_t *CIDACin);
  void Reset_FIFO(struct tCAN_FIFO *FIFO);
  uint8_t CAN_TRANSMITTER(void);
  uint8_t CAN_TRANSMITTER_EMPTY(void);

  void Reset_Data(void);
  //void CAN_Reset(void);

  void NMT_Receive(CanRxMsgTypeDef Message);  //tCAN_msg Message
  void SYNC_Receive(CanRxMsgTypeDef Message);  //tCAN_msg Message
  void TIME_Receive(CanRxMsgTypeDef Message);  //tCAN_msg Message
  void LIVEGUARD_Receive(CanRxMsgTypeDef Message); //tCAN_msg Message
  void RXPDO_Receive(uint8_t PDO, CanRxMsgTypeDef Message); //,tCAN_msg Message
  void TXPDO_Receive(uint8_t PDO, CanRxMsgTypeDef Message); //tCAN_msg Message
  void RXSDO_Receive(CanRxMsgTypeDef Message); //tCAN_msg Message
  //void LSS_Receive(CanRxMsgTypeDef Message);//tCAN_msg Message
  void Send_NODEGUARD(uint8_t Toggle, uint8_t Status);
  void Send_trans_IRQ1(CanRxMsgTypeDef Message, uint8_t scs, uint32_t data);
  void Send_trans_IRQ2(uint8_t scs, uint8_t* Adresse);
  void Send_Register_unknown(CanRxMsgTypeDef Message);

  uint8_t CAN_setup(uint8_t FIFO, uint32_t baudrate);
  uint32_t CAN_Controllerstatus(void);
  uint8_t Canopen_Register_Read(uint32_t *Registerset, uint32_t size,
      uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size);
  void Send_Segmented_Upload_Responce(CanRxMsgTypeDef Message, uint32_t Adresse,
      uint32_t Size);
  void StopServices(void);
  void LSS_Receive(CanRxMsgTypeDef Message);
  void Send_Segmented_Download_Responce(CanRxMsgTypeDef Message,
      uint32_t Adresse, uint32_t Size);
public:
  CAN_HandleTypeDef* fhcan;
  TIM_HandleTypeDef* fhtim;

  uint8_t Kaltstart_request;
  uint8_t Reset_Communication;
  uint8_t* RPDO[4][8];
  uint8_t RPDO_remember_mask[4][8];
  uint8_t* RPDO_Data_New[4]; // 1 Bit pro byte;
  uint32_t Last_CAN_Error;
  uint8_t Open;

  tUnican(CAN_HandleTypeDef *hcan, tServoEEPROM *EEPROM, TIM_HandleTypeDef *htim);
  ~tUnican(void);
  uint16_t fMyInstanceIndex;
  void CAN_Error(void);

  uint8_t Handle_Coldstart(void);
  void CAN_EnterCritical(uint32_t *buffer);
  void CAN_LeaveCritical(uint32_t *buffer);

  //uint8_t OpenCan(UNICAN_InitTypeDef* UNICAN_InitStruct, uint8_t Def_Node_ID, uint16_t rate, char const* c_NAME, char const* c_HW_VER, char const* c_SW_VER);  // ID = 0 CAN-Loop Back Mode
  uint8_t OpenCan(uint8_t Def_Node_ID, char const* c_NAME,
      char const* c_HW_VER, char const* c_SW_VER);  // ID = 0 CAN-Loop Back Mode
  void CAN_Receiver(void);
  uint16_t BootloaderVersion(void);
  uint8_t BUSOFF(void);
  uint8_t Connected(void);
  void Handle_Emergency(void);
  void Runtime_Statistik(void);
  uint8_t Handle_TPDO(uint8_t TPDO_Number, uint8_t sync, uint8_t size,
      uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4,
      uint8_t byte5, uint8_t byte6, uint8_t byte7);
  //void SetI2CHandel(I2C_HandleTypeDef *fhi2c);
  uint32_t Get_Error_Betriebsstunden(uint8_t n);
  uint8_t Set_Error_Betriebsstunden(uint16_t Error_code);
  void Call_CIA_TIMERS(void);
  tSERVO_EEPROM_Default GetEEPROMRegleparameters(uint8_t Achse);
  void SetEEPROMRegleparameters(uint8_t Achse, tSERVO_EEPROM_Default Data);

  int32_t Get_software_position_limit(uint8_t n);
  void Set_software_position_limit(uint8_t n, int32_t value);
};


#endif
