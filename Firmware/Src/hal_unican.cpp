/**
  ******************************************************************************
  * File Name          : unican.cpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  ******************************************************************************
  **/
#include "hal_unican.hpp"
//#include "main.hpp"
//#include "eeprom.h"
//#include  "stm32f1xx_hal_can.h"


#define USE_EEPROM_FIFO 1



/*CLed::CLed(GPIO_TypeDef* port,uint16_t pin,uint16_t toggleTime)
{
  _port=port;
  _pin=pin;
  _toggleTime=toggleTime;
  counter=0;
  off();
}

void CLed::runToggle()
{
  if(++counter>=_toggleTime)
  {
    counter=0;
    toggle();
  }
}*/
// extern void Error_Handler();

/*ToDo: ist das schlau so?*/
//#define EEPROM_ADD_Errors 0x0005
//NB_OF_VAR
//uint16_t VirtAddVarTab[NB_OF_VAR] = {EEPROM_ADD_Errors, 0x6666, 0x7777};
tEEPROM_Registers *EManReg = ( tEEPROM_Registers*) EEADR_EManReg_Area;



#define BOOTLOADER_BASE  (uint32_t) FLASH_BASE+0x80000

#define VariablenRegistersCount 9
tCANopenRegisterLUT VariablenRegisters[VariablenRegistersCount];
const tCANopenRegisterLUT DEF_CANopenRegisterLUT = {0,0,1,0,4};



//tSERVO_EEPROM_Default *Achse0 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_0_Parameter_B;
//tSERVO_EEPROM_Default *Achse1 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_1_Parameter_B;
//tSERVO_EEPROM_Default *Achse2 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_2_Parameter_B;
//tSERVO_EEPROM_Default *Achse3 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_3_Parameter_B;

tSERVO_EEPROM_Registers *SERVO_0 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_0_Parameter_B;
tSERVO_EEPROM_Registers *SERVO_1 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_1_Parameter_B;
tSERVO_EEPROM_Registers *SERVO_2 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_2_Parameter_B;
tSERVO_EEPROM_Registers *SERVO_3 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_3_Parameter_B;
//tUnican* handlers[2];

uint16_t volatile* SERVO_Registers[10] =  // Ohne Offset
{
  (uint16_t volatile*)(0x24),  // 0 S2_VSOLL_LS   (Test 0x824 f�r 0A24)
  (uint16_t volatile*)(0x26),  // 1 S2_ASOLL_LS    _S2_LP(0x26)
  (uint16_t volatile*)(0x28),  // 2 S2_IMAX        _S2_LP(0x28)
  (uint16_t volatile*)(0x38),  // 3 S2_IMIN        _S2_LP(0x38)
  (uint16_t volatile*)(0x0A),  // 4 S2_KI          _S2_P(0x0B)
  (uint16_t volatile*)(0x0C),  // 5 S2_KR          _S2_P(0x0D)
  (uint16_t volatile*)(0x1C),  // 6 S2_TI          _S2_LP(0x1C)
  (uint16_t volatile*)(0x2C),  // 7 S2_TD          _S2_LP(0x2C)
  (uint16_t volatile*)(0x3C),  // 8 S2_LRP         _S2_P(0x3D)
  (uint16_t volatile*)(0x0E)   // 9 S2_CONFIG      _S2_LP(0x0E)
};
#define ConstantenRegistersCount 11
const tCANopenRegisterLUT ConstantenRegisters[ConstantenRegistersCount] = {
  { FlashOperation, 0x00, 0, 5 ,1},                                                               // 1
  { FlashOperation, FlashStartAdress, 0, FLASH_BASE, sizeof((uint32_t *)FLASH_BASE) },            // 2
  { FlashOperation, FlashSize, 0, STM32_FLASH_SIZE, sizeof((uint32_t *)STM32_FLASH_SIZE) },       // 3
  { FlashOperation, FlashPageSize, 0, STM32_PAGE_SIZE, sizeof((uint32_t *)STM32_PAGE_SIZE) },     // 4
  { ExtFlashOperation, 0x00, 0, 5 ,1},
  { ExtFlashOperation, FlashStartAdress, 0, 0, 4},
  { ExtFlashOperation, FlashSize, 0, 0, 4},
  { ExtFlashOperation, FlashPageSize, 0, 0, 4},
  { MemoryOrganisation, 0, 0, 20,  1},
  { MemoryOrganisation, 1, 0, FLASH_BASE , 4},
  { MemoryOrganisation, 2, 0, STM32_FLASH_SIZE, 4} /*,
  { MemoryOrganisation, 3, 0, CCMDATARAM_BASE, 4},
  { MemoryOrganisation, 4, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 5, 0, SRAM1_BASE, 4},
  { MemoryOrganisation, 6, 0, 0x20000, 4},   // (112 KB)fdgdfgsdfgdfgdfg
  { MemoryOrganisation, 7, 0, SRAM2_BASE, 4},
  { MemoryOrganisation, 8, 0, 0x4000, 4},   // (16 KB)
  { MemoryOrganisation, 9, 0, SRAM3_BASE, 4},
  { MemoryOrganisation, 10, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 11, 0, APB1PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 11, 0, APB2PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},   // (64 KB)    32KB CODE SIZE Begrenzung
  { MemoryOrganisation, 11, 0, AHB1PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 11, 0, AHB2PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},  // (64 KB)
  { MemoryOrganisation, 13, 0, BKPSRAM_BASE, 4},
  { MemoryOrganisation, 14, 0, 0x1000, 4},   // (4 KB)
  { MemoryOrganisation, 19, 0, FSMC_R_BASE, 4},
  { MemoryOrganisation, 20, 0, 0x1000, 4}   */
};

#define SC_Flash_Enabled        (uint32_t) 0x01
#define SC_NMT_STOPPED          (uint32_t) 0x02
#define SC_Bootloader_Enabled   (uint32_t) 0x04
#define SC_Toggle_Bit           (uint32_t) 0x10
#define SC_NODEGUARD_Toggle_Bit (uint32_t) 0x20
#define SC_Segmented_Upload     (uint32_t) 0x40
#define SC_Segmented_Download   (uint32_t) 0x80

#define CAN_SOFT_FIFO 1

#if  CAN_SOFT_FIFO == 1
  tCAN_FIFO           TXC_FIFO;
#else
  tCAN_FIFO_STATISTIK RXC_FIFO, TXC_FIFO;
#endif

extern uint8_t FPGA_Mirror[];

uint32_t Debug;
static tUnican* handlers[MAX_HANDLERS] = {0};
static uint16_t  fInstanceCounter = 0;

void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* hcan)
{
  uint8_t i;
  for (i=0; i<fInstanceCounter; i++)
  {
    if (handlers[i]->fhcan == hcan)
    {
      handlers[i]->CAN_Receiver();
      if (HAL_CAN_Receive_IT(hcan, CAN_FIFO0) != HAL_OK) // Empfang wieder starten
      {
        /* Reception Error */
        /* Fehler k�nnte ein HAL_LOCK des Transmitters sein */
        /*ToDo: warum immerr error????*/
        //Error_Handler();
        handlers[i]->CAN_Error();
      }
    }
  }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan)
{
  uint8_t i;
  for (i=0; i<fInstanceCounter; i++)
  {
    if (handlers[i]->fhcan == hcan)
    {
      handlers[i]->CAN_Error();
      if (HAL_CAN_Receive_IT(hcan, CAN_FIFO0) != HAL_OK)
      {
        //* Nach Fehler nicht mehr los?
        handlers[i]->CAN_Error();
      }
    }
  }
}


#ifdef __cplusplus
 extern "C" {
#endif
void(*SysMemBootJump) (void);

void CiaBootLoader(uint32_t BootloaderStatus)
{
  uint32_t SP   = *((uint32_t *) 0x0808C000);
  SysMemBootJump=(void (*)(void)) (*((uint32_t*) 0x0808C004));
  if (BootloaderStatus == 1)
  {
     RCC->CIR = 0x00000000;  // Disable Interrups
     __set_PRIMASK(1);       // Disable Interrups
     __set_MSP(SP);          // Defaultwert
     SysMemBootJump();
     while(1);
  }
}

uint32_t Intervall(uint32_t min, uint32_t Value, uint32_t max)
{
  if (min > Value) return min;
  if (max < Value) return max;
  return Value;
}

#ifdef __cplusplus
}
#endif

 void BootLoaderInit(uint32_t BootloaderStatus)
 {
   uint32_t SP   = *((uint32_t *) 0x1fff0000);               // Wert aus dem ROM
   SysMemBootJump=(void (*)(void)) (*((uint32_t*) 0x1fff0004));   // Zeiger aus dem ROM
   if (BootloaderStatus == 1)
   {
      //RCC_DeInit();
      HAL_RCC_DeInit();
      SysTick->CTRL = 0;
      SysTick->LOAD = 0;
      SysTick->VAL = 0;

      __set_PRIMASK(1);          // Disable Interrups
      __set_MSP(SP);             // Wert aus dem ROM
      //__set_MSP(0x20001000);  // Defaultwert

      SysMemBootJump();

      while(1);
   }
 }

__weak uint8_t CUSTOM_Register_Write(uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size)
{
  return 0;
}

__weak uint8_t CUSTOM_Register_Read(uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size)
{
  return 0;
}

 __weak int16_t SPI_CANopen_Receive16(uint8_t Adress)
 {
   return 0;
 }
 __weak void SPI_CANopen_Send16(uint8_t Adress, uint16_t Data)  // Register folowing space 0x200 - 0x27F
 {

 }

__weak void OnCUSTOM_Register_Read_DONE(void)
{

}

__weak void OnCUSTOM_Register_Write_DONE(void)
{

}


void tUnican::Reset_Data()
{
  uint8_t i,j;
  Open                      = 0;
  Kaltstart_request = 0;

  for (i=0;i<4;i++)
  {
  for (j=0;j<8;j++)
  {
    RPDO[i][j]               = &fNUL;
    RPDO_remember_mask[i][j] = 0x00;
  }
  RPDO_Data_New[i] = &fNUL;
  fTPDO_enabled[i]  = false;
  fSend_sync_PDO[i] = false;
  fTPDO_enabled_old[i] = false;
  fSoftware_position_limit[i << 1]     = 0x80000000;
  fSoftware_position_limit[1+(i << 1)] = 0x7FFFFFFF;
  }

  fEmergency_Error_code     = No_Error;
  fEmergency_Error_code_old = Generic_Error;
  fError_register = 0;
  fError_register_old       = Generic; // Senden Ausl�sen
  fStatus_register          = 0x00000000;
  fStatus_register_old      = 0x00000001;
  fControlword              = 0x0000;
  fStatusword               = 0x0000;
  fDevice_select            = true;
  fHeartbeatTime            = 0;
  fState_Control            = SC_Preoperational;
  fCycleMax                 = 0;
  fCycleMin                 = 0xFFFF;
  fRX_Errors                = 0;
  fTX_Errors                = 0;


   for (i=1; i< VariablenRegistersCount; i++)
      VariablenRegisters[i] = DEF_CANopenRegisterLUT;

   VariablenRegisters[0].CANopenReg = 0x1000;
   VariablenRegisters[0].IsPointer  = 0;

   VariablenRegisters[1].CANopenReg = 0x1001;
   VariablenRegisters[1].Length     = 2;
   VariablenRegisters[1].Value      = (uint32_t) &fError_register;

   VariablenRegisters[2].CANopenReg = 0x1002;
   VariablenRegisters[2].Length     = 2;
   VariablenRegisters[2].Value      = (uint32_t) &fStatus_register;

   VariablenRegisters[3].CANopenReg = 0x1008;
   VariablenRegisters[3].Value      = (uint32_t) fHW_NAME;

   VariablenRegisters[4].CANopenReg = 0x1009;
   VariablenRegisters[4].Value      = (uint32_t) fHW_VER;

   VariablenRegisters[5].CANopenReg =0x1018;
   VariablenRegisters[5].IsPointer  = 0;
   VariablenRegisters[5].Value      = 4;

   VariablenRegisters[6].CANopenReg = 0x1018;
   VariablenRegisters[6].SubIndex   = 4;
   VariablenRegisters[6].Value      = (uint32_t) &fSerial_Number;

   VariablenRegisters[7].CANopenReg = 0x6040;
   VariablenRegisters[7].Value      = (uint32_t) &fControlword;

   VariablenRegisters[8].CANopenReg = 0x6041;
   VariablenRegisters[8].Value      = (uint32_t) &fStatusword;



   /*ToDo: war drin
   Reset_Communication = true;*/

 }

 uint16_t tUnican::BootloaderVersion(void)
 {
   #if defined(STM32F10X_MD) || defined(STM32F103xB)
   return 0;  // nur 128kb Flash
   #else
   uint32_t  ptr, sum = 0;
   uint16_t result = 0;

   {
     for (ptr = 0x0808C000;ptr < 0x0808E000;ptr++)
       sum = sum + *(uint8_t *) ptr;
     switch (sum)
     {
       case 753825  : result = 0x0100; break;
       case 743838  : result = 0x0101; break;
       case 754782  : result = 0x0102; break;
       case 2088960 : result = 0x0000; break; // alles FF
       case 2088705 : result = 0x0000; break; // fast alles FF
       default      : result = 0x0001; break; // unbekannter Inhalt
     }
   }
   return (result);
   #endif
 }

 uint8_t tUnican::Canopen_Register_Read(uint32_t *Registerset, uint32_t size, uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size)
 {
   uint8_t i, len;
   uint32_t SetSize ;
   SetSize = size / sizeof(tCANopenRegisterLUT);


   for (i=0;i<SetSize;i++)
   {
     if ((Register == ((tCANopenRegisterLUT*)Registerset)[i].CANopenReg) && (Sub == ((tCANopenRegisterLUT*)Registerset)[i].SubIndex))
     {
       if (((tCANopenRegisterLUT*)Registerset)[i].IsPointer)
         *Data = * ((uint32_t*) (((tCANopenRegisterLUT*)Registerset)[i].Value));
       else
         *Data = ((tCANopenRegisterLUT*)Registerset)[i].Value;
       len = ((tCANopenRegisterLUT*)Registerset)[i].Length;
       if (len == 1) *Data &= 0xFF;
       if (len == 2) *Data &= 0xFFFF;
       if (len == 3) *Data &= 0xFFFFFF;
       return 1;
     }
   }
   return 0;
 }


 void tUnican::Send_NODEGUARD(uint8_t Toggle, uint8_t Status)
 {
	struct tcan_buf tempBuf;
	tempBuf.id_field = fNode_ID + FUNCTION_CODE_LIVEGUARD;
	tempBuf.dlc = 1;
    if(Toggle)
	  tempBuf.data_field[0]= (uint8_t) 0x80 | Status;
    else
	  tempBuf.data_field[0]= (uint8_t) 0x7F & Status;

    CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0);
 }

 void tUnican::Send_trans_IRQ1(CanRxMsgTypeDef Message, uint8_t scs, uint32_t data)
 {
   struct tcan_buf tempBuf;
   tempBuf.id_field = fNode_ID + FUNCTION_CODE_SDO_TX;
   tempBuf.dlc = 8;

   tempBuf.data_field[0]     = scs;
   tempBuf.data_field[1]     = Message.Data[1];
   tempBuf.data_field[2]     = Message.Data[2];
   tempBuf.data_field[3]     = Message.Data[3];
   tempBuf.data_field[4]     = data;
   tempBuf.data_field[5]     = data>>8;
   tempBuf.data_field[6]     = data>>16;
   tempBuf.data_field[7]     = data>>24;

   CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0);
 }
 void tUnican::Send_trans_IRQ2(uint8_t scs, uint8_t* Adresse)
 {
   //tCAN_msg  OUT_Message;
   uint8_t   i ;
   struct tcan_buf tempBuf;
   uint16_t  Data = 0;
   uint16_t  Size = 7-((scs >> 1) & 0x07);
   // Die SPI Variante biegt den Register folowing Space auf sie SPI Schnittstelle um

   tempBuf.id_field = FUNCTION_CODE_SDO_TX | fNode_ID;
   tempBuf.dlc = 8;
   tempBuf.data_field[0] = scs;
   for (i=0;i<7;i++)
   {
	 if (i >= Size)
	   tempBuf.data_field[i+1] = 0;
	 else if (((uint32_t)Adresse >= 0x200) &&  ((uint32_t)Adresse < 0x280))
	 {
	   if ((i == 0) || !((uint32_t)(Adresse + i) & 1))
	   Data = SPI_CANopen_Receive16((uint32_t)Adresse + i);
	   if ((uint32_t)(Adresse + i) & 1)
		 tempBuf.data_field[i+1] = Data;
	   else
		 tempBuf.data_field[i+1] = Data >> 8;

	 }
	 else
	   tempBuf.data_field[i+1] = *(Adresse + i);
   }


    if (!CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0))
    {
      fError_register           |= Communication;
      fEmergency_Error_code      = CAN_Overrun + 3;
    }

 }

void tUnican::Send_Register_unknown(CanRxMsgTypeDef Message)
 {
   Send_trans_IRQ1(Message,ABORT_DOMAIN_TRANSFER , 0x060200013);
 }

void tUnican::Send_Segmented_Upload_Responce(CanRxMsgTypeDef Message, uint32_t Adresse, uint32_t Size)
{
  Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_DEFINED_IN_DATA , Size);
  fBytes_to_Send    = Size;
  fState_Control &= ~SC_Toggle_Bit;
  fState_Control |= SC_Segmented_Upload;
  fPointer_for_Send = (uint8_t *)Adresse;
}

void tUnican::Send_Segmented_Download_Responce(CanRxMsgTypeDef Message, uint32_t Adresse, uint32_t Size)
{
  Send_trans_IRQ1(Message, INITIATE_DOWNLOAD_RESPONCE, Size);
  fBytes_to_Send    = Size;
  fState_Control &= ~SC_Toggle_Bit;
  fState_Control |= SC_Segmented_Download;
  fPointer_for_Send = (uint8_t *)Adresse;
}

/*----------------------------------------------------------------------------
  CAN NMT receive
 *----------------------------------------------------------------------------*/
void tUnican::NMT_Receive(CanRxMsgTypeDef Message){
  uint8_t i;
  uint32_t Zufall;

  //if ((Message.Data[0] == 0xAD)  && (Message.DLC == 2))
     //CAN_Reset();  // LP Tot !!!
  if ((Message.Data[1] == fNode_ID) && (Message.DLC == 2))
  {
    switch (Message.Data[0])
    {
      case 0x82 : Reset_Communication = true; break;                // Reset Comunication
      case 0x80 : for (i=0;i<4;i++)                                // Pre Operal
                    fTPDO_enabled[i]   = fTPDO_enabled_old[i] = false;
                  fState_Control &= ~SC_NMT_STOPPED;
                  fState_Control |= SC_Preoperational;
                  break;
      case 0x81 : Kaltstart_request = true; break;   // Reset
      case 0x01 : for (i=0;i<4;i++)                                 // Operal
                  {
                    fTPDO_enabled[i]    = true;
                    fSend_sync_PDO[i] = false;
                  }
                  if ((fSerial_Number & 0xFF0000) == 0)
                  {
                    //STM32F1
                    //Zufall  = STM32_UUID[0];
                    //Zufall  = Zufall ^ STM32_UUID[1];
                    //Zufall  = Zufall ^ STM32_UUID[2];
                    Zufall  = Unique_ID1 ^ Unique_ID2 ^ Unique_ID3;
                    Zufall ^= (Zufall >> 8);
                    Zufall &= 0x00FFFFFF;
                    Zufall |= 0x07000000;
                    fSerial_Number = Zufall;
                    pCANEEPROM->EEStoreValue(EEADR_Serial_Number, Zufall, 4);

                  }
                  fState_Control &= ~SC_NMT_STOPPED;
                  fState_Control &= ~SC_Preoperational;
                  break;
      case 0x02 : for (i=0;i<4;i++)                                 // Stopped
                    fTPDO_enabled[i] = fTPDO_enabled_old[i] = false;
                  fState_Control |= SC_NMT_STOPPED;
                  fState_Control &= ~SC_Preoperational;
                  break;
    }
  }
}


/*----------------------------------------------------------------------------
  CAN SYNC receive
 *----------------------------------------------------------------------------*/
void tUnican::SYNC_Receive(CanRxMsgTypeDef Message){
  uint8_t i;
    for (i=0;i<4;i++)
      fSend_sync_PDO[i] = true;
}
/*----------------------------------------------------------------------------
  CAN TIME receive
 *----------------------------------------------------------------------------*/
void tUnican::TIME_Receive(CanRxMsgTypeDef Message){
  tTIME  TIME_OF_DAY;
  TIME_OF_DAY.miliseconds = (Message.Data[3] << 24) + (Message.Data[2] << 16) + (Message.Data[1] << 8) + Message.Data[0];
  TIME_OF_DAY.days        = (Message.Data[5] << 8) + Message.Data[4];
  TIME_OF_DAY.miliseconds = TIME_OF_DAY.miliseconds & (unsigned long) 0x0FFFFFFF;
  fBirthday = TIME_OF_DAY.days;

  if (fBirthday == 0xFFFF)
  {
    pCANEEPROM->EEStoreValue(EEADR_Birthday , TIME_OF_DAY.days, 2);
    fBirthday =  pCANEEPROM->EEReadValue(EEADR_Birthday, 2);
    fBirthday = TIME_OF_DAY.days;
  }
}

/*----------------------------------------------------------------------------
  CAN LIVEGUARD receive
 *----------------------------------------------------------------------------*/
void tUnican::LIVEGUARD_Receive(CanRxMsgTypeDef Message){
  if (!fHeartbeatTime && Message.RTR) //  RTR = true; else RTR = false;
  {
    if (fState_Control & SC_NMT_STOPPED)
      Send_NODEGUARD(fState_Control & SC_NODEGUARD_Toggle_Bit, 4);   // Stopped
    else
      if (fState_Control & SC_Preoperational)
    	Send_NODEGUARD(fState_Control & SC_NODEGUARD_Toggle_Bit, 127); // Pre Operal
      else
    	Send_NODEGUARD(fState_Control & SC_NODEGUARD_Toggle_Bit, 5);   // Operal

    if (fState_Control & SC_NODEGUARD_Toggle_Bit)
      fState_Control &= ~SC_NODEGUARD_Toggle_Bit;
    else
      fState_Control |=  SC_NODEGUARD_Toggle_Bit;
  }
}

void tUnican::LSS_Receive(CanRxMsgTypeDef Message)
{
  // Layer-Setting-Service
  // CANopen den Layer-Setting-Service (LSS) in CiA DSP-305. Mit
  //Hilfe des Layer Setting Service (LSS) kann ein LSS-Master die
  //Baudrate und Knotennummer eines LSS-Slaves �ber den CAN-Bus
  //�ndern.
  switch (Message.Data[0])
  {
    case 0x04:
       switch (Message.Data[1])
       {
         case 0 : //Operationsmodus einschalten
         case 1 : //Konfigurationsmodus einschalten
         default :;
       }
       break;
    case 0x13:
       if (Message.Data[1] == 0) // Baudratentabelle, die nach CiA DSP-305
       switch (Message.Data[2])
       {
         case 0 : // 1000kBit
         case 1 : // 800kBit
         case 2 : // 500kBit
         case 3 : // 250kBit
         case 4 : // 125kBit
         case 5 : // 100kBit
         case 6 : // 50kBit
         case 7 : // 20kBit
         case 8 : // 10kBit Send message (0x13, 0 ,0 , x , x, x, x, x) //  erfolgreich ausgef�hrt
         default : ; // Send message (0x13, 1 ,0 , x , x, x, x, x) // Baudrate wird nicht unterst�tzt
       }
       break;
    case 0x15:  // relative Zeit bis zum Einschalten der neuen Baudrate in ms
         // Delay = (Message.data[5] << 2) + Message.data[1];
       break;
    case 0x11:
       // New Node ID = Message.data[1];
       // Send message (0x11, 0 ,0 , x , x, x, x, x) //  erfolgreich ausgef�hrt
       break;

  }
}

/*----------------------------------------------------------------------------
  CAN RXPDO receive
 *----------------------------------------------------------------------------*/
void tUnican::RXPDO_Receive(uint8_t PDO, CanRxMsgTypeDef Message){
  uint8_t i;
  for (i=0;i<Message.DLC;i++)
    *RPDO[PDO][i] = (*RPDO[PDO][i] & RPDO_remember_mask[PDO][i]) | Message.Data[i];
  *RPDO_Data_New[PDO] = 0xFF;
}

/*----------------------------------------------------------------------------
  CAN TXPDO receive
 *----------------------------------------------------------------------------*/

void tUnican::TXPDO_Receive(uint8_t PDO, CanRxMsgTypeDef Message){
  fTPDO_enabled_old[PDO] = !Message.RTR;  // RTR = type
}

/*----------------------------------------------------------------------------
  CAN RXSDO receive
 *----------------------------------------------------------------------------*/

void tUnican::RXSDO_Receive(CanRxMsgTypeDef Message){
  uint16_t Register;
  uint8_t  SubIndex;
  uint32_t Data_32;
  uint16_t Data_16;
  uint16_t Temp = 0;
  //uint16_t Status = 0;
  uint8_t  ccs,i;
  uint32_t Size;
  uint8_t  RegKnown, StandardAnswer, StartSegmented;

  ccs      = Message.Data[0];
  Size     = 4 - ((ccs >> 2) & 3);
  Register = (Message.Data[2] << 8) + Message.Data[1];
  SubIndex = Message.Data[3];
  Data_16  = (Message.Data[5] << 8) + Message.Data[4];
  Data_32  = (Message.Data[7] << 24) + (Message.Data[6] << 16) + Data_16;

  switch(ccs & CONTROL_BYTE_CCS_MASK)
  {
    case INITIATE_DOWNLOAD_REQUEST  : /* initiate download request */
    {
      RegKnown       = false;
      StandardAnswer = true;
      StartSegmented = true;
      if ((ccs & SDO_xCS_SIZE_CONTROL_MASK) == SDO_xCS_SIZE_DEFINED_IN_DATA)
        Size = Data_32;
      // else if ((ccs & SDO_xCS_SIZE_CONTROL_MASK) == SDO_xCS_SIZE_UNDEFINED)
      //  Size = 0;
      else // Unsegmentet
      {
        if ((ccs & SDO_xCS_SIZE_CONTROL_MASK) == SDO_xCS_SIZE_UNDEFINED)
          Size = 0;
        StartSegmented = false;
        switch ((uint16_t)Register)
        {
          case 0x1003 : // Reset Errors
            if ((SubIndex == 0x00) && (Message.Data[4] == 0x00))
            {
               pCANEEPROM->EEStoreValue(EEADR_Errors , 0, 2);
              RegKnown = true;
            }
          break;
          case 0x1011 : // Reset Parameters
            if ((SubIndex == 0x01) && (Message.Data[4] == 0x01))
            {
               pCANEEPROM->Set_EEPROM((uint32_t) (EEADR_EEData_OK)    , 0xFF);
              RegKnown          = true;
              Kaltstart_request = true;
            }
          break;
          case 0x1017 : // HeartbeatTime
            if (SubIndex == 0x00)
            {
              fHeartbeatTime  = Data_16;
              RegKnown = true;
            }
          break;
          case 0x6040 : // controlword
            if (SubIndex == 0x00)
            {
              fControlword = Data_16;
              RegKnown = true;
            }
          break;
          case 0x607D: // ARRAY software_position_limit Integer32 rw O
          {
            i = SubIndex - 1;
            if ((i & 0x07) == i)
            {
               fSoftware_position_limit[i] = Data_32;
               RegKnown = true;
            }
          }
          break;
          case Set_Serial_number : // Manufacturer Specific Object
            if (SubIndex == 0x00)
            {
               pCANEEPROM->EEStoreValue(EEADR_Serial_Number, Data_32, 4);
              RegKnown = true;
            }
          break;
          case Select_Device_SN : // Manufacturer Specific Object
            if (SubIndex == 0x00)
            {
              if (!(Data_32 == fSerial_Number))
              fDevice_select = false;
              RegKnown        = true;
            }
          break;
          case Set_New_Node_ID : // Manufacturer Specific Object
            if (SubIndex == 0x00)
            {
               pCANEEPROM->Set_EEPROM((uint32_t) EEADR_CanOpen_Node_ID,(Message.Data[4]) & 0x7F);
              RegKnown = true;
              Kaltstart_request = true;
            }
          break;
          case Set_SERVO_0_Direct : // Manufacturer Specific Object
            if (SubIndex < 9) // Kein Config register schreiben
            {
  // SPI Variante
               SPI_CANopen_Send16((uint32_t)SERVO_Registers[SubIndex], Data_16);
              RegKnown = true;
            }
          break;
          case Set_SERVO_0_EEPROM : // Manufacturer Specific Object: A-Achse
            if (SubIndex < 10)
            {
               pCANEEPROM->EEStoreValue((uint32_t) (&(SERVO_0 -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_SERVO_1_Direct : // Manufacturer Specific Object
            if (SubIndex < 9) // Kein Config register!
            {
  // SPI Variante
              SPI_CANopen_Send16((uint32_t)SERVO_Registers[SubIndex] + 0x80, Data_16);
              RegKnown = true;
            }
          break;
          case Set_SERVO_1_EEPROM : // Manufacturer Specific Object: A-Achse
            if (SubIndex < 10)
            {
               pCANEEPROM->EEStoreValue((uint32_t) (&(SERVO_1 -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_ManReg_EEPROM : // Manufacturer Specific Object
            if (SubIndex < EManReg_Area_size)
            {
               pCANEEPROM->EEStoreValue((uint32_t) (&(EManReg -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_HC12_Register_Byte :
            if ((Data_16 >= HC12_EEPROM_BASE) && ((Data_16 < (HC12_EEPROM_BASE + EEPROM_SIZE))))
               pCANEEPROM->Set_EEPROM(Data_16 - HC12_EEPROM_BASE  ,0xFF & (Data_32 >> 16));
            else
              *(uint8_t *) (Data_32) = 0xFF & (Data_32 >> 16);
            RegKnown = true;
          break;
          case FlashOperation :
            switch (SubIndex)
            {
              case FlashPageCommand :
                if (Data_16 == 0x79)
                {
                  fState_Control |= SC_Bootloader_Enabled;
                  RegKnown = true;
                }
              break;
            }
          break;
          default:

            RegKnown = ((CUSTOM_Register_Write != NULL) && CUSTOM_Register_Write(Register, SubIndex , &Data_32, &Size));
            if (RegKnown)
               OnCUSTOM_Register_Write_DONE();
          break;
        }
      }
      if (StartSegmented)
      {
        fCurrent_SDO_Register = Register;
        // HC12 Releikt
        if (((uint16_t)Register == HC12_EEPROM_BASE))
        {
          StandardAnswer     = false;
        }
        else if (((uint16_t)Register == 0x8000))
        {
          Send_Segmented_Download_Responce(Message, 0, Size);
          // CAN_EnterCritical(&IER_Store);
          //Send_trans_IRQ1(Message, INITIATE_DOWNLOAD_RESPONCE, Data_32);
          fState_Control |= SC_Flash_Enabled;
          StandardAnswer     = false;
        }
        else if (((CUSTOM_Register_Write != NULL) && CUSTOM_Register_Write(Register, SubIndex , &Data_32, &Size)))
        {
          Send_Segmented_Download_Responce(Message, Data_32, Size);
          StandardAnswer     = false;
        }

      }
      if (StandardAnswer)
      {
        if (RegKnown)
          Send_trans_IRQ1(Message, INITIATE_DOWNLOAD_RESPONCE, 0x00);
        else
          Send_Register_unknown(Message);
      }
    }
    break;      /* initiate download request */
    case INITIATE_UPLOAD_REQUEST : /* initiate upload request */
    {
      RegKnown = true;
      switch ((uint16_t)Register)  // signed ist hier wichtig sonst funzt 8000 nicht mehr
      {
        case 0x1003 : // manufacturer status register UNSIGNED32 ro O
          Data_32 = Get_Error_Betriebsstunden(SubIndex);
          if (SubIndex < 11)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, Data_32); // (Errors[Message.data[3]] + ((uint32_t) Error_Betriebsstunden[Message.data[3]] << 16))    );
          else
            RegKnown = false;
        break;
        case 0x100A : // manufacturer software version Vis-String const O
          switch (SubIndex)
          {
            case 0  : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, * (uint32_t *) fSW_VER); break;
            case 1  : Temp = 0x3030+ (SPI_CANopen_Receive16(0xFF&(int8_t)0x023E));
                      Temp = (Temp >> 8) + (Temp << 8); // Change Order
                      Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,Temp);
                      break;      // FPGA Software
            case 2  : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, * (uint32_t *) UNICAN_VERSION ); break;
            case 3  : Temp = 0x3030+fBL_VER;
                      Temp = (Temp >> 8) + (Temp << 8); // Change Order
                      Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, Temp ); break;
            default : RegKnown = false;
          }
        break;

        case Set_ManReg_EEPROM : // Manufacturer Specific Object
           if (SubIndex < EManReg_Area_size)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  pCANEEPROM->EEReadValue((uint32_t)(&(EManReg -> Data[SubIndex])), 2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_0_Direct : // Manufacturer Specific Object: A-Achse
           if (SubIndex < 10)
// SPI Variante

            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, SPI_CANopen_Receive16((uint32_t)SERVO_Registers[SubIndex]));
          else
            RegKnown = false;
        break;
        case Set_SERVO_0_EEPROM : // Manufacturer Specific Object: X-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  pCANEEPROM->EEReadValue((uint32_t) &SERVO_0 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_1_Direct : // Manufacturer Specific Object: X-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, SPI_CANopen_Receive16((uint32_t)SERVO_Registers[SubIndex] + 0x80));
          else
            RegKnown = false;
        break;
        case Set_SERVO_1_EEPROM : // Manufacturer Specific Object: Y-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  pCANEEPROM->EEReadValue((uint32_t) &SERVO_1 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;

        case Set_SERVO_2_EEPROM : // Manufacturer Specific Object: R-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  pCANEEPROM->EEReadValue((uint32_t) &SERVO_2 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_3_EEPROM : // Manufacturer Specific Object: A-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  pCANEEPROM->EEReadValue((uint32_t) &SERVO_3 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;
        case Get_Betriebsstunden : // Manufacturer Specific Object:
          if (SubIndex < 10)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  pCANEEPROM->EEReadValue(EEADR_Betriebsstunden, 2) );
          else
            RegKnown = false;
        break;
        case Get_Birtday : // Manufacturer Specific Object:
         if (SubIndex < 10)
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  pCANEEPROM->EEReadValue(EEADR_Birthday,2));
          else
            RegKnown = false;
        break;
        case Get_CYCLE_State : // Manufacturer Specific Object:
          switch (SubIndex)
          {
            case 0 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE, 2); break;
            case 1 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_2BYTE, fCycleMin); break;
            case 2 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_2BYTE, fCycleMax); break;
            default : RegKnown = false;
          }
          break;
        case Get_FIFO_State : // Manufacturer Specific Object:
          switch (SubIndex)
          {
            case 0 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE, 6); break;
            case 1 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE, 0 /*RXC_FIFO.FIFO_Filling*/); break;
            case 2 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE, 0 /*RXC_FIFO.FIFO_Max_Filling*/); break;
            case 3 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE, TXC_FIFO.FIFO_Filling); break;
            case 4 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE, TXC_FIFO.FIFO_Max_Filling); break;
            case 5 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_2BYTE, fRX_Errors); break;
            case 6 : Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_2BYTE, fTX_Errors); break;
            default : RegKnown = false;
          }
        break;
        case 0x607D : // ARRAY software_position_limit Integer32 rw O
          i = SubIndex - 1;
          if ((i & 0x07) == i)
             Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, fSoftware_position_limit[i]);
          else
            RegKnown = false;
        break;
        default:
          // HC12 Releikt
          if (((uint16_t)Register == 0x0000) || ((uint16_t)Register == 0x0200)  || ((uint16_t)Register == 0x0800) || ((uint16_t)Register == HC12_EEPROM_BASE) || ((uint16_t)Register == 0x8000))
          {
            if ((uint16_t)Register == 0x0000)
            {
              Data_32 = ((uint32_t) APB1PERIPH_BASE); // Adresse
              Size             = 0x200;
            }
            if ((uint16_t)Register == 0x0200)
            {
              Data_32 = ((uint32_t) FPGA_Mirror); // Adresse
              Size               = 0x200;
            }

            if ((uint16_t)Register == 0x0800)
            {
              Data_32 = ((uint32_t) SRAM_BASE); // Adresse  //fr�her SRAM1_BASE
              Size             = 0x3FF;
            }
            if ((uint16_t)Register == HC12_EEPROM_BASE)
            {
              Data_32 = ((uint32_t)  pCANEEPROM->GetMirrorAdr()); // Adresse
              Size             = 0x300;
            }
            if ((uint16_t)Register == 0x8000)
            {
              Data_32 = ((uint32_t) FLASH_BASE);  // ALT !
              Size             =  BOOTLOADER_BASE - FLASH_BASE;
            }
            Send_Segmented_Upload_Responce(Message, Data_32 , Size);
          }

          else if (Canopen_Register_Read((uint32_t*)&ConstantenRegisters, sizeof(ConstantenRegisters), Register, SubIndex, &Data_32, &Size))
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_4BYTE | (((4-Size) & 3) << 2) , Data_32);
          else if (Canopen_Register_Read((uint32_t*)&VariablenRegisters, sizeof(VariablenRegisters), Register, SubIndex, &Data_32, &Size))
            Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_4BYTE | (((4-Size) & 3) << 2) , Data_32);
          else if (CUSTOM_Register_Read(Register, SubIndex, &Data_32, &Size))
          {
            if (Size > 4)
              Send_Segmented_Upload_Responce(Message, Data_32 , Size);
            else
            {
              Send_trans_IRQ1(Message, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_4BYTE | (((4-Size) & 3) << 2) , Data_32);
              OnCUSTOM_Register_Read_DONE();
            }
          }
          else
            RegKnown = false;
        break;
      }
      if (!RegKnown)
        Send_Register_unknown(Message);
    }
    break; /* initiate upload request */
    case DOWNLOAD_SEGMENT_REQUEST : /* segmented download request    EEPROM brennen */
      if (fState_Control & SC_Segmented_Download)
      {
        Size = 7 - ((ccs >> 1) & 0x7);

        if (Size == 0) // Nicht Spezifiziert
          Size = 7;
        for (i=1;((i <= Size) && (i <= (fBytes_to_Send)));i++)
        {
          if ((fCurrent_SDO_Register == HC12_EEPROM_BASE) && (((uint32_t)fPointer_for_Send) < (uint32_t) EEPROM_SIZE))
             pCANEEPROM->Set_EEPROM((uint32_t) fPointer_for_Send ,Message.Data[8-i]);
          else
            *fPointer_for_Send = Message.Data[i];
          fPointer_for_Send = fPointer_for_Send + 1;
        }

        Message.Data[1] = Message.Data[2] = Message.Data[3] = 0;
        Send_trans_IRQ1(Message, DOWNLOAD_SEGMENT_RESPONCE | (fState_Control & SC_Toggle_Bit), 0); // e = 0, 32kB

        if (fState_Control & SC_Toggle_Bit) fState_Control &= ~SC_Toggle_Bit;
        else                               fState_Control |=  SC_Toggle_Bit; // = SEGMENTED_TOGGLE_BIT

        if ((fBytes_to_Send < 7) || ((ccs & SEGMENTED_NO_MORE_SEGMENTS) != 0))
        {
          fState_Control &= ~SC_Segmented_Download;
          OnCUSTOM_Register_Write_DONE();
        }
        fBytes_to_Send    -= 7;
      }
      else
        Send_Register_unknown(Message);
    break;
    case UPLOAD_SEGMENT_REQUEST : /* segmented upload request */
      if (fState_Control & SC_Segmented_Upload )
      {
        if (fBytes_to_Send < 8)
        {
          Send_trans_IRQ2(UPLOAD_SEGMENT_RESPONCE | SEGMENTED_NO_MORE_SEGMENTS | (fState_Control & SC_Toggle_Bit) | ((7-fBytes_to_Send) << 1), fPointer_for_Send); // e = 0, 32kB
          fState_Control &= ~SC_Segmented_Upload;
          OnCUSTOM_Register_Read_DONE();
        }
        else
        {
          Send_trans_IRQ2(UPLOAD_SEGMENT_RESPONCE | (fState_Control & SC_Toggle_Bit) | SEGMENTED_7BYTES , fPointer_for_Send); // e = 0, 32kB
          fBytes_to_Send    -= 7;
          fPointer_for_Send += 7;
        }
        if (fState_Control & SC_Toggle_Bit) fState_Control &= ~SC_Toggle_Bit;
        else                                fState_Control |=  SC_Toggle_Bit; // = SEGMENTED_TOGGLE_BIT
      }
      else
        Send_Register_unknown(Message);
    break;
    case ABORT_DOMAIN_TRANSFER : /* Abort SDO Transfer Protocol */
             fState_Control &=  ~SC_Segmented_Upload;
    break;
    default:
      Send_Register_unknown(Message);
    break;
  }
}


/*----------------------------------------------------------------------------
  CAN Receiver receive
 *----------------------------------------------------------------------------*/
void tUnican::CAN_Receiver(void)
{
  uint8_t number;

  if (fDevice_select)
  {
    if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_NMT)
      NMT_Receive(*fhcan->pRxMsg);
    else if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_SYNC)
      SYNC_Receive(*fhcan->pRxMsg);
    else if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_TIME)
      TIME_Receive(*fhcan->pRxMsg);
    else if (((fhcan->pRxMsg->StdId & ID_MASK) == fNode_ID) && (fhcan->pRxMsg->StdId >= FUNCTION_CODE_PDO_TX_BASE))
    {  /* Peer_to_Peer */
      number = (fhcan->pRxMsg->StdId - FUNCTION_CODE_PDO_TX_BASE) >> 8;
      if (number < 4)
      {
        if ((fhcan->pRxMsg->StdId & FUNCTION_CODE_SYNC) == 0)
          RXPDO_Receive(number, *fhcan->pRxMsg);
        else
          TXPDO_Receive(number,*fhcan->pRxMsg);    // RTR Objekte
      }
      else
      {
        if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_SDO_RX)
          RXSDO_Receive(*fhcan->pRxMsg);
        else if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_LIVEGUARD)
          LIVEGUARD_Receive(*fhcan->pRxMsg);
        else if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_LSS)
          LSS_Receive(*fhcan->pRxMsg);
      }
    }
  }
  else
  {
    if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_NMT)
      fDevice_select = true;
   if ((fhcan->pRxMsg->StdId & FUNCTION_MASK) == FUNCTION_CODE_SDO_RX)
      fDevice_select = true;
  }

}

void tUnican::StopServices(void)
{
  #if (USE_EEPROM_FIFO == 1)
  while(! (CAN_TRANSMITTER_EMPTY() &&  pCANEEPROM->EEPROM_WRITER_DONE()))
  {
     pCANEEPROM->EEPROM_WRITER();
     CAN_TRANSMITTER();
  }
  #endif
  //CAN_EnterCritical(&IER_Store);
  //while (!CAN_checkMailboxEmpty (fMyCanCtrl, Standard_Mailbox, &LastError))
  while (!CAN_TRANSMITTER_EMPTY())
    CAN_TRANSMITTER();
}

/*----------------------------------------------------------------------------
  public declarations
 *----------------------------------------------------------------------------*/
tUnican::tUnican(CAN_HandleTypeDef *hcan, tServoEEPROM *EEPROM, TIM_HandleTypeDef *htim)
{
  fMyInstanceIndex = fInstanceCounter;
  handlers[fInstanceCounter++] = this; // register this instance against the id

  //static CanTxMsgTypeDef TxMessage;
  //static CanRxMsgTypeDef RxMessage;
  Open = false;
  fhcan = hcan;
  pCANEEPROM = EEPROM;
  fhcan->pTxMsg = &TxMessage;
  fhcan->pRxMsg = &RxMessage;

  fhtim = htim;


  Reset_FIFO(&TXC_FIFO);
  Reset_Data();

 //SysTick->VAL;

}

tUnican::~tUnican(void)
{
  Reset_Data();
  fInstanceCounter--;
  for (int i=fMyInstanceIndex; i<fInstanceCounter; i++)
	handlers[i] = handlers[i+1];
  // fifo muss auf den HEAP und hier wieder gekillt werden;

}


/*----------------------------------------------------------------------------
  Disable / Enable CAN Interrupts
 *----------------------------------------------------------------------------*/
//macht die main....

uint8_t tUnican::OpenCan(uint8_t Def_Node_ID, char const* c_NAME, char const* c_HW_VER, char const* c_SW_VER)
{
  CAN_FilterConfTypeDef  sFilterConfig;
  uint8_t                vERROR_Code;

  fBL_VER   = BootloaderVersion();
  fHW_NAME  = c_NAME;
  fHW_VER   = c_HW_VER;
  fSW_VER   = c_SW_VER;
  fSerial_Number = 0;
  vERROR_Code = 0;

  // CAN_Reset(); Quatsch weil wir dannach MX_CAN2_Init(); aufrufen m�ssten

  if (vERROR_Code == 0)  //solange keine Baudrate gesetzt wird egal...
  {
    //init_I2C1_EEPROM(UNICAN_InitStruct->I2cCtrl, UNICAN_InitStruct->I2cGPIOx, UNICAN_InitStruct->I2cGPIO_PinBits);
    fNode_ID         =  pCANEEPROM->Get_EEPROM((uint32_t) EEADR_CanOpen_Node_ID);
    fSerial_Number   =  pCANEEPROM->EEReadValue(EEADR_Serial_Number, 4);
    fBirthday        =  pCANEEPROM->EEReadValue(EEADR_Birthday, 2);
    fBetriebsstunden =  pCANEEPROM->EEReadValue(EEADR_Betriebsstunden, 2);

    if (( pCANEEPROM->Get_EEPROM((uint32_t) EEADR_EEData_OK) != 0xAD) || (fNode_ID == 0x00) || (fNode_ID & 0x80))
    {
      fNode_ID = Def_Node_ID;
       pCANEEPROM->Set_EEPROM((uint32_t) EEADR_EEData_OK,0xAD);
       pCANEEPROM->Set_EEPROM((uint32_t) EEADR_CanOpen_Node_ID,fNode_ID);
      if (fSerial_Number == (uint32_t)0xFFFFFFFF)
         pCANEEPROM->EEStoreValue(EEADR_Serial_Number, 0x07000000, 4);
      if (fBetriebsstunden == 0xFFFF)
         pCANEEPROM->EEStoreValue(EEADR_Betriebsstunden, 0x0000 , 2);
      if ( pCANEEPROM->EEReadValue(EEADR_Errors,2) == 0xFFFF)
         pCANEEPROM->EEStoreValue(EEADR_Errors, 0x0000 , 2);
    }

    /*ToDo: geht das auch ohne, weil Filtersettting unten*/
    //Filter Setting
    /*CAN_wrFilter (fMyCanCtrl, RXFIFO, FUNCTION_CODE_NMT, 0 , STANDARD_FORMAT);
    CAN_wrFilter (fMyCanCtrl, RXFIFO, FUNCTION_CODE_SYNC , 0, STANDARD_FORMAT);
    CAN_wrFilter (fMyCanCtrl, RXFIFO, FUNCTION_CODE_TIME , 0, STANDARD_FORMAT);
    CAN_wrFilter (fMyCanCtrl, RXFIFO, fNode_ID, (uint32_t)0x780 , STANDARD_FORMAT);*/

    sFilterConfig.FilterNumber = 0;
    sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
    sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
    sFilterConfig.FilterIdHigh = 0x0000;
    sFilterConfig.FilterIdLow = 0x0000;
    sFilterConfig.FilterMaskIdHigh = 0x0000;
    sFilterConfig.FilterMaskIdLow = 0x0000;
    sFilterConfig.FilterFIFOAssignment = 0;
    sFilterConfig.FilterActivation = ENABLE;
    sFilterConfig.BankNumber = 0x2d;//14;

    if (HAL_CAN_ConfigFilter(fhcan, &sFilterConfig) != HAL_OK)
    {
      /* Filter configuration Error */
      Reset_Communication  = true;
    }
    else
      Reset_Communication  = false;

    //fNode_ID = Def_Node_ID;//0x051;//Def_Node_ID;
    fhcan->pTxMsg->StdId = fNode_ID;
    fhcan->pTxMsg->ExtId = 0x01;
    fhcan->pTxMsg->RTR = CAN_RTR_DATA;
    fhcan->pTxMsg->IDE = CAN_ID_STD;
    fhcan->pTxMsg->DLC = 2;


    //__HAL_CAN_ENABLE_IT(fhcan, CAN_FIFO0);//CAN_IT_FMP0);
    if (HAL_CAN_Receive_IT(fhcan, CAN_FIFO0) != HAL_OK)
      {
        /* Reception Error */
    //    Error_Handler();
      }

    Reset_Data();


    fHeartbeatTimer             = 10;
    while(fHeartbeatTimer);       // �berm��igen Aktionismus bremsen

       // CRIER |= 0x01;             // Enable Recive Interrupt
    Send_NODEGUARD(0,0);      // Bootup Protokoll


    //Serial_Number = Get_EEPROM((uint32_t) EEADR_Serial_Number + 0, 4);
    //if (!CAN_start (fMyCanCtrl))
    //  vERROR_Code = 0x02;
/*ToDo: ist das gut oder kann das weg*/
    if (HAL_CAN_GetState(fhcan)!= HAL_CAN_STATE_READY)
      vERROR_Code = 0x02;
  }

  /*ToDo: brauch ich das Timer setzen?
  RCC_TIMPeriphClock_Cmd(STATISTIK_TIMER, ENABLE);

  TIM_TimeBase_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBase_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBase_InitStructure.TIM_Period   = 32767;
  TIM_TimeBase_InitStructure.TIM_Prescaler = 21;    // 4 MHz
  TIM_TimeBaseInit(STATISTIK_TIMER, &TIM_TimeBase_InitStructure);
  TIM_Cmd(STATISTIK_TIMER, ENABLE);*/

  // Test29BIT();
  Open = true;
  return (vERROR_Code == 0);
}

void tUnican::Call_CIA_TIMERS (void)
{
  if (fHeartbeatTimer) fHeartbeatTimer--;
  if (fHeartbeatTime && !fHeartbeatTimer)
  {
    if (fState_Control & SC_NMT_STOPPED)  Send_NODEGUARD(0, 4);   // Stopped
    else if (fState_Control & SC_Preoperational)  Send_NODEGUARD(0, 127); // Pre OPERATIONAL
    else                                          Send_NODEGUARD(0, 5);   // OPERATIONAL
    fHeartbeatTimer = fHeartbeatTime; // Heartbeat Protokoll
  }

  fLivetimer ++;
  if (fLivetimer > 999)
  {
    fLivetimer_sekunden++;
    fLivetimer = 0;
  }

  pCANEEPROM->EEPROM_TIMER_CALL_EVERY_MS();

}

uint8_t tUnican::Handle_Coldstart()
{
  if (fState_Control & SC_Flash_Enabled)
  {

  }

  if (fState_Control & SC_Bootloader_Enabled)
  {
    while (!CAN_TRANSMITTER_EMPTY())
      CAN_TRANSMITTER();

    __disable_irq();
    BootLoaderInit(1);
  }

  if (Kaltstart_request && CAN_TRANSMITTER_EMPTY() && pCANEEPROM->EEPROM_WRITER_DONE()) /*&& fMemmory.WRITER_DONE()*/
  {
    Reset_Communication = true;
    return 1;
  }
  else
    return 0;

}

tSERVO_EEPROM_Default tUnican::GetEEPROMRegleparameters(uint8_t Achse)
{
  uint8_t i;
  uint16_t vData;

  tSERVO_EEPROM_Default result;
  for (i=0;i<(sizeof(result)/sizeof(uint16_t));i++)
  {
    vData =  pCANEEPROM->EEReadValue(EEADR_SERVO_0_Parameter_B + (Achse * 20) + 2*i,2);
    *( ((uint16_t*)(&result)) + i) = vData;
  }
  return result;
}

void tUnican::SetEEPROMRegleparameters(uint8_t Achse, tSERVO_EEPROM_Default Data)
{
  uint8_t i;
  uint16_t vData;

  for (i=0;i<(sizeof(Data)/sizeof(uint16_t));i++)
  {
    vData = *( ((uint16_t*)(&Data)) + i);
     pCANEEPROM->EEStoreValue(EEADR_SERVO_0_Parameter_B + (Achse * 20) + 2*i, vData , 2);
  }
}

uint8_t tUnican::Handle_TPDO(uint8_t TPDO_Number, uint8_t sync, uint8_t size, uint8_t byte0, uint8_t byte1, uint8_t byte2,
                          uint8_t byte3, uint8_t byte4, uint8_t byte5, uint8_t byte6, uint8_t byte7)
{
  //CanTxMsgTypeDef  Message;
  uint8_t  i, TPDO_enabled_temp, Result, Send, Index;
  uint8_t Data[8];
  struct tcan_buf tempBuf;
  Result = true;

  if ((TPDO_Number >= 1) && (TPDO_Number <= 4))
  {
    Index             = TPDO_Number - 1;             // Index Initialisieren
    TPDO_enabled_temp = fTPDO_enabled[Index];         // Zustand einfrieren
    if (TPDO_enabled_temp)
    {
	  Send     = 0;
	  Data[0] = byte0;
	  Data[1] = byte1;
	  Data[2] = byte2;
	  Data[3] = byte3;
	  Data[4] = byte4;
	  Data[5] = byte5;
	  Data[6] = byte6;
	  Data[7] = byte7;

	  if (sync == 1)                                // sync  PDO
	  {
	    if (fSend_sync_PDO[Index])
	    {
		  Send                   = true;
		  fSend_sync_PDO[Index] = false;
	    }
	  }
	  else                                         // event PDO
	  {
	    Send = !fTPDO_enabled_old[Index];
	    for (i = 0; i < 8; i++)
	    {
	      if (fTPDO_DATA_OLD[TPDO_Number - 1][i] != Data[i])
		  {
		    Send = true;
		    break;
		  }
	    }
	  }

	  if (Send)
      {


        tempBuf.id_field = (0x0080 | fNode_ID) + (TPDO_Number << 8);
        tempBuf.dlc = size;
        for (i = 0; i < 8; i++)
          tempBuf.data_field[i] = fTPDO_DATA_OLD[TPDO_Number - 1][i] =  Data[i];

        if (CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0))
        {
          fTPDO_enabled_old[Index] = TPDO_enabled_temp;
        }
        else
          Result = false;    // Kanal war belegt
      }
    }
  }
  return (Result);
}

void tUnican::Handle_Emergency(void)
{
  //CanTxMsgTypeDef  Message;
  uint8_t i;
  struct tcan_buf tempBuf;
  fCAN_Controller_Errorregister = CAN_Controllerstatus();
  // LEC[6:4]
  //000: No Error
  //001: Stuff Error
  //010: Form Error
  //011: Acknowledgment Error
  //100: Bit recessive Error
  //101: Bit dominant Error
  //110: CRC Error
  //111: Set by software

  if (BUSOFF())
     fEmergency_Error_code = Error_Passive_Mode;

  CAN_TRANSMITTER();
  Runtime_Statistik();


	if ((fEmergency_Error_code != fEmergency_Error_code_old) ||
		(fError_register       != fError_register_old))
	{
	  tempBuf.id_field = fNode_ID + FUNCTION_CODE_EMERGENCY;
	  tempBuf.dlc = 8;
	  for (i = 0; i < 8; i++)
		  tempBuf.data_field[i] = 0;
	  tempBuf.data_field[0]          = fEmergency_Error_code << 8;
	  tempBuf.data_field[1]          = fEmergency_Error_code;
	  tempBuf.data_field[2]          = fError_register;


	  if (CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0))
	  {
		fEmergency_Error_code_old  = fEmergency_Error_code;
		fError_register_old        = fError_register;


		if (fEmergency_Error_code)
		  Set_Error_Betriebsstunden(fEmergency_Error_code);
	  }

  }

  if (fState_Control & SC_Flash_Enabled)
  {
    StopServices();
    CiaBootLoader(1);

    /*
    while (1)
    {
      while (CAN2->RF0R & CAN_RF0R_FMP0)
      {
        CAN_rdMsg (fMyCanCtrl, RXFIFO, &CAN_RxMsg[fMyCanCtrl-1]);
        Receiver(fMyCanCtrl);
      }
      CAN2->RF0R |= CAN_RF0R_FOVR0;
      CAN2->RF0R |= CAN_RF0R_FULL0;
    }
    */
  }

}


 // fhcan->Errorcode
 // HAL_CAN_ERROR_NONE      0x00000000U    /*!< No error             */
 // HAL_CAN_ERROR_EWG       0x00000001U    /*!< EWG error            */
 // HAL_CAN_ERROR_EPV       0x00000002U    /*!< EPV error            */
 // HAL_CAN_ERROR_BOF       0x00000004U    /*!< BOF error            */
 // HAL_CAN_ERROR_STF       0x00000008U    /*!< Stuff error          */
 // HAL_CAN_ERROR_FOR       0x00000010U    /*!< Form error           */
 // HAL_CAN_ERROR_ACK       0x00000020U    /*!< Acknowledgment error */
 // HAL_CAN_ERROR_BR        0x00000040U    /*!< Bit recessive        */
 // HAL_CAN_ERROR_BD        0x00000080U    /*!< LEC dominant         */
 // HAL_CAN_ERROR_CRC       0x00000100U    /*!< LEC transfer error   */
 // HAL_CAN_ERROR_FOV0      0x00000200U    /*!< FIFO0 overrun error  */
 // HAL_CAN_ERROR_FOV1      0x00000400U    /*!< FIFO1 overrun error  */
 // HAL_CAN_ERROR_TXFAIL    0x00000800U    /*!< Transmit failure     */
 //

void tUnican::CAN_Error(void)
{
  if (fhcan->ErrorCode & ~512)
    Last_CAN_Error = CAN_Controllerstatus();

}

uint8_t tUnican::BUSOFF(void)
{
  return CAN_Controllerstatus() & CAN_ESR_BOFF;
}


/* Statistikwerte werden in 1/8 ms angegeben */
void tUnican::Runtime_Statistik(void)
{
  uint32_t Timer;
  float    Delta;


if (fhtim == NULL)
{
  Timer = SysTick->LOAD-SysTick->VAL; //STATISTIK_TIMER->CNT;
  if (fCycleMax > 0)
  {
    fCycleTime = Timer - fRT_Timer;
    if (Timer < fRT_Timer)
      fCycleTime += SysTick->LOAD;
    Delta = fCycleTime;
    fCycleTime = (Delta * 8000000) / HAL_RCC_GetHCLKFreq();
    if (fCycleTime > fCycleMax)
      fCycleMax = fCycleTime;
    if (fCycleTime < fCycleMin)
      fCycleMin = fCycleTime;


  } else
  {
    fCycleMax  = 1;
    fRT_Timer  = Timer;
  }
  fRT_Timer  = Timer;
}
else
{
  Timer = fhtim->Instance->CNT;
    if (fCycleMax > 0)
    {
      fCycleTime = Timer - fRT_Timer;
      fCycleTime *= 2; //  (auf 8MHz)
      if (fCycleTime > fCycleMax)
        fCycleMax = fCycleTime;
      if (fCycleTime < fCycleMin)
        fCycleMin = fCycleTime;


    } else
    {
      fCycleMax  = 1;
      fRT_Timer  = Timer;
    }
    fRT_Timer  = Timer;

}

  // Betriebsstunden Z�hler
  if ((fLivetimer_sekunden == 3600) || (fLivetimer_sekunden == 7200))
  {
    fLivetimer_sekunden++;
    fBetriebsstunden++;
  }

  if (fLivetimer_sekunden > 10801)
  {
    fBetriebsstunden++;
    pCANEEPROM->EEStoreValue(EEADR_Betriebsstunden, fBetriebsstunden , 2);
    fLivetimer_sekunden = 0;
  }

  #if (USE_EEPROM_FIFO == 1)
     pCANEEPROM->EEPROM_WRITER();
  #endif

}


uint32_t tUnican::Get_Error_Betriebsstunden(uint8_t n)
{
  // W�chst nach kleineren Zahlen
  uint8_t First_in, Last_in, count, Point;
  uint32_t  Result;
  First_in = Intervall(1, pCANEEPROM->EEReadValue(EEADR_Errors , 1) & 0xFF,10);
  Last_in  = Intervall(1, pCANEEPROM->EEReadValue(EEADR_Error_Betriebsstunden , 1) & 0xFF ,10);
  if ((( pCANEEPROM->EEReadValue(EEADR_Errors , 1) & 0xFF) == 0) || (n > (Error_Betriebsstunden_Size - 1))) // Abw�rtskompatibel
    return 0;
  else if (First_in >= Last_in)
  count = 1 + First_in - Last_in;
  else
    count = (Error_Betriebsstunden_Size + First_in) - Last_in;
  if (n == 0)
    return count;
  else if (n <= count)
  {
    Point  = Last_in + n - 1;
    if (Point > (Error_Betriebsstunden_Size - 1))
      Point = 1 + Point - (Error_Betriebsstunden_Size);
    Result =  pCANEEPROM->EEReadValue(EEADR_Errors + 2*Point, 2) + ( pCANEEPROM->EEReadValue(EEADR_Error_Betriebsstunden + 2*Point, 2) << 16);
    return Result;
  }
  else return 0;
}

uint8_t tUnican::Set_Error_Betriebsstunden(uint16_t Error_code)
{
  // W�chst nach kleineren Zahlen
  unsigned char Last_in, First_in;
  if ( pCANEEPROM->EEPROM_WRITER_DONE())
  {
    First_in = Intervall(1, pCANEEPROM->EEReadValue(EEADR_Errors , 1) & 0xFF,10);
    Last_in  = Intervall(1, pCANEEPROM->EEReadValue(EEADR_Error_Betriebsstunden , 1) & 0xFF ,10);
    if (( pCANEEPROM->EEReadValue(EEADR_Errors , 1) & 0xFF) != 0)
    {
      Last_in  = Last_in - 1;
      if (Last_in == 0)
        Last_in = (Error_Betriebsstunden_Size - 1);
      if (First_in == Last_in)
        First_in = First_in - 1;
      if (First_in == 0)
        First_in = (Error_Betriebsstunden_Size - 1);
    }
    else
      Last_in = First_in; // Das Erste nach dem L�schen
     pCANEEPROM->EEStoreValue(EEADR_Errors + 2*Last_in, Error_code, 2);
     pCANEEPROM->EEStoreValue(EEADR_Error_Betriebsstunden + 2*Last_in, fBetriebsstunden, 2);
     pCANEEPROM->EEStoreValue(EEADR_Errors, First_in, 1);
     pCANEEPROM->EEStoreValue(EEADR_Error_Betriebsstunden, Last_in, 1);
    return true;
  }
  else return false;
}

int32_t tUnican::Get_software_position_limit(uint8_t n)
{
  if (n<8)
    return fSoftware_position_limit[n];
  else
    return 0;
}

void  tUnican::Set_software_position_limit(uint8_t n, int32_t value)
{
  if (n<8)
    fSoftware_position_limit[n] = value;
}


uint8_t tUnican::Connected(void)
{
  return (fState_Control & SC_Preoperational) != 0;
}


uint32_t tUnican::CAN_Controllerstatus(void)
{
  //CAN_TypeDef *pCAN = (ctrl == 1) ? CAN1 : CAN2;
  uint32_t result;
  uint8_t  RXFIFO_Messages;
  // Zur Info
  RXFIFO_Messages  = fhcan->Instance->RF0R & (uint32_t) 0x03;
  RXFIFO_Messages += fhcan->Instance->RF1R & (uint32_t) 0x03;

  result  = fhcan->Instance->ESR;
  if (fhcan->Instance->RF1R & CAN_RF1R_FOVR1) result |= (uint32_t) 0x8000;   // FIFO1_Overrun
  if (fhcan->Instance->RF1R & CAN_RF1R_FULL1) result |= (uint32_t) 0x4000;   // FIFO1_Warning
  if (fhcan->Instance->RF0R & CAN_RF0R_FOVR0) result |= (uint32_t) 0x2000;   // FIFO1_Overrun
  if (fhcan->Instance->RF0R & CAN_RF0R_FULL0) result |= (uint32_t) 0x1000;   // FIFO1_Warning
  return(result);
}


uint8_t tUnican::CAN_TRANSMITTER(void)
{
  tcan_buf txBuf;
  uint8_t Not_used, i;
  uint8_t Result = true;

  if (Open && CAN_TRANSMITTER_EMPTY())
  {
    if (CAN_FIFO_GET(&TXC_FIFO, &txBuf, &Not_used))
    {

      fhcan->pTxMsg->StdId = txBuf.id_field;
      fhcan->pTxMsg->ExtId = 0x01;
      fhcan->pTxMsg->IDE   = CAN_ID_STD;
      // HC12 RTR = ((*(unsigned short *) &(Receive_temp.id_field)) & (unsigned short) 0x0010);
      fhcan->pTxMsg->RTR   = CAN_RTR_DATA;
      fhcan->pTxMsg->DLC   = txBuf.dlc;
      for (i=0;i<8;i++)
        fhcan->pTxMsg->Data[i]     = txBuf.data_field[i];

      uint32_t prim;
      prim = __get_PRIMASK();
      __disable_irq();
      Result = (HAL_CAN_Transmit_IT(fhcan) == HAL_OK);
      if (!prim)
      __enable_irq();

    }
  }
  // Sometimes when HAL_CAN_Transmit_IT Locked FHCAN Receiver not restarted from IRQ
  if (!CAN_FIFO0)
  {
     if (Open && (fhcan -> State != HAL_CAN_STATE_BUSY_RX0_RX1) && (fhcan -> State != HAL_CAN_STATE_BUSY_TX_RX0)  && (fhcan -> State != HAL_CAN_STATE_BUSY_RX0))
       HAL_CAN_Receive_IT(fhcan,CAN_FIFO0);
     if (Open && !(fhcan -> Instance->IER & CAN_IER_FMPIE0))
       HAL_CAN_Receive_IT(fhcan,CAN_FIFO0);
  }
  else
  {
	 if (Open && (fhcan -> State != HAL_CAN_STATE_BUSY_RX0_RX1) && (fhcan -> State != HAL_CAN_STATE_BUSY_TX_RX1)  && (fhcan -> State != HAL_CAN_STATE_BUSY_RX1))
	   HAL_CAN_Receive_IT(fhcan,CAN_FIFO1);
	 if (Open && !(fhcan -> Instance->IER & CAN_IER_FMPIE1))
	   HAL_CAN_Receive_IT(fhcan,CAN_FIFO1);
  }

  return (Result);
}

uint8_t tUnican::CAN_TRANSMITTER_EMPTY(void)
{
  return !((fhcan->State == HAL_CAN_STATE_BUSY_TX) ||
           (fhcan->State == HAL_CAN_STATE_BUSY_TX_RX0) ||
           (fhcan->State == HAL_CAN_STATE_BUSY_TX_RX1));
}

uint8_t tUnican::CAN_FIFO_GET(struct tCAN_FIFO *FIFO,struct tcan_buf *Data, uint8_t *CIDACin)
{
  struct tcan_buf *temp;
  uint8_t Pointer_temp;
  uint8_t result = false;
  uint32_t prim;
  prim = __get_PRIMASK();
    __disable_irq();
  if (FIFO -> FIFO_out_Pointer != FIFO -> FIFO_in_Pointer)
  {     
     Pointer_temp = FIFO -> FIFO_out_Pointer;
     temp = &(FIFO -> can_buf[Pointer_temp]);
     *Data = *temp; 
     *CIDACin = Data -> unused[0];
     FIFO -> FIFO_out_Pointer = Pointer_temp + 1; 
     if (FIFO -> FIFO_Filling > FIFO -> FIFO_Max_Filling)
       FIFO -> FIFO_Max_Filling = FIFO -> FIFO_Filling;
     FIFO -> FIFO_Filling     = FIFO -> FIFO_Filling - 1;
     if (FIFO -> FIFO_out_Pointer >= CAN_FIFO_Size)
       FIFO -> FIFO_out_Pointer = 0;
     result = true;
  }
  if (!prim)
    __enable_irq();
  return result;
}

uint8_t tUnican::CAN_FIFO_ADD(struct tCAN_FIFO *FIFO, struct tcan_buf *Data, uint8_t CIDACin)
{
  uint8_t result, Pointer_temp;
  struct tcan_buf *temp;
  result = false;
  uint32_t prim;
  prim = __get_PRIMASK();
  __disable_irq();

  if (FIFO ->FIFO_in_Pointer < CAN_FIFO_Size) // Schutz
  {
    Pointer_temp = FIFO -> FIFO_in_Pointer;
    temp = &(FIFO -> can_buf[Pointer_temp]);  
    FIFO -> FIFO_in_Pointer = Pointer_temp + 1;
    if (FIFO -> FIFO_in_Pointer >= CAN_FIFO_Size)
      FIFO -> FIFO_in_Pointer = 0;
    if (FIFO -> FIFO_in_Pointer == FIFO -> FIFO_out_Pointer)
      FIFO -> FIFO_in_Pointer = Pointer_temp;  // result false;
	  else // Daten schreiben
  	{
  	  *temp = *Data;                               // Bei �berlauf wird das letzte Paket Priori�t verworfen
      temp -> unused[0]     = CIDACin;             // Bei �berlauf wird das letzte Paket Priori�t verworfen
      FIFO -> FIFO_Filling  = FIFO -> FIFO_Filling + 1;  
      result = true;
	  }
  }
  else
    Reset_FIFO(FIFO); // result false;
  if (!prim)
    __enable_irq();
  return result;
}

void tUnican::Reset_FIFO(struct tCAN_FIFO *FIFO)
{
  uint32_t prim;
  prim = __get_PRIMASK();
    __disable_irq();
  FIFO -> FIFO_in_Pointer   = 0;
  FIFO -> FIFO_out_Pointer  = 0;
  FIFO -> FIFO_Filling      = 0;
  FIFO -> FIFO_Max_Filling  = 0;
  if (!prim)
    __enable_irq();
}
