/*
 * ServoEEPROM.cpp
 *
 *  Created on: 28.02.2017
 *      Author: Hildebrandt
 */

#include "hal_IIC_EEPROM.hpp"
#include "string.h"

tServoEEPROM* tServoEEPROM::handlers[MAX_HANDLERS] = {0};
uint16_t tServoEEPROM::fInstanceCounter = 0;


tServoEEPROM::tServoEEPROM(int Size)
{
  fMyInstanceIndex = fInstanceCounter;
  handlers[fInstanceCounter++] = this; // register this instance against the
  fEEpromMirrorSize = Size;
  fSerialEEPROMConnect = false;
  pEEpromReadMirror  = new uint8_t[fEEpromMirrorSize];
  pEEpromWriteMirror = new uint8_t[fEEpromMirrorSize];
  EEPROM_Timer = 0;
}

tServoEEPROM::~tServoEEPROM(void)
{
  fEEpromMirrorSize = 0;
  fSerialEEPROMConnect = false;
  delete [] pEEpromReadMirror;
  delete [] pEEpromWriteMirror;
  fInstanceCounter--;
  for (int i=fMyInstanceIndex; i<fInstanceCounter; i++)
    handlers[i] = handlers[i+1];
}

uint8_t* tServoEEPROM::GetMirrorAdr(void)
{
  return pEEpromWriteMirror;
}

uint8_t tServoEEPROM::ConnectSerialEEPROM(I2C_HandleTypeDef* hI2C)
{
  uint16_t i;
  uint8_t result;
  uint8_t Data;
  EEPROM_FIFO_STATE = EEPROM_FIFO_WS_IDLE;
  pMyhI2C = hI2C;

  result =  HAL_I2C_IsDeviceReady(pMyhI2C, MEM_DEVICE_WRITE_ADDR,5,I2C_TIMEOUT_MAX);
  for (i=0;i<fEEpromMirrorSize;i++)
  {
    if (result != HAL_OK)
      break;
    result = HAL_I2C_Mem_Read(pMyhI2C,  MEM_DEVICE_WRITE_ADDR , i, _MemAddSize,  &Data, sizeof(Data), I2C_TIMEOUT_MAX);
    Set_EEPROMMIRRROR(i, Data);
    fSerialEEPROMConnect = (result == HAL_OK);
  }
  return (result == HAL_OK);
}


uint8_t tServoEEPROM::EEPROM_WRITER_DONE(void)
{
  uint16_t vAddr;
  return Check_EEPROMMIRRROR(&vAddr) && (EEPROM_FIFO_STATE == EEPROM_FIFO_WS_IDLE) && (__HAL_I2C_GET_FLAG(pMyhI2C, I2C_FLAG_BUSY) != SET);
}


uint8_t tServoEEPROM::Get_EEPROMMIRROR_WIRTE(uint16_t Addr)
{
  if (Addr < fEEpromMirrorSize)
    return pEEpromWriteMirror[Addr];
  else
    return 0;
}

uint8_t tServoEEPROM::Get_EEPROM(uint16_t Addr)
{
  if (Addr < fEEpromMirrorSize)
    return pEEpromReadMirror[Addr];
  else
    return 0;
}


void tServoEEPROM::Set_EEPROM(uint16_t Addr, uint8_t value)
{
  if (Addr < fEEpromMirrorSize)
    pEEpromWriteMirror[Addr] = value;
}

void tServoEEPROM::EEStoreValue(uint16_t Addr, uint32_t Value, uint8_t count) // LSB FIRST STEHT IM EEPROM ?
{
  uint8_t  Offset = 0;
  if (count > 4)
      count = 4;
  while (count--)
  {
    Set_EEPROM((uint16_t)Addr + Offset++, Value & 0xFF);
    Value = Value >> 8;
  }
}
uint32_t tServoEEPROM::EEReadValue(uint16_t Addr, uint8_t count) // LSB FIRST STEHT IM EEPROM ?
{
  uint32_t Value  = 0;
  if (count > 4)
    count = 4;
  while (count--)
  {
    Value = Value << 8;
    Value = Value + Get_EEPROM((uint16_t)Addr + count);
  }
  return Value;
}


tSERVO_EEPROM_Default tServoEEPROM::GetEEPROMRegleparameters(uint8_t Achse)
{
  uint8_t i;
  uint16_t vData;

  tSERVO_EEPROM_Default result;
  for (i=0;i<(sizeof(result)/sizeof(uint16_t));i++)
  {
    vData = EEReadValue(EEADR_SERVO_0_Parameter_B + (Achse * 20) + 2*i,2);
    *( ((uint16_t*)(&result)) + i) = vData;
  }
  return result;
}

void tServoEEPROM::SetEEPROMRegleparameters(uint8_t Achse, tSERVO_EEPROM_Default Data)
{
  uint8_t i;
  uint16_t vData;

  for (i=0;i<(sizeof(Data)/sizeof(uint16_t));i++)
  {
    vData = *( ((uint16_t*)(&Data)) + i);
    EEStoreValue(EEADR_SERVO_0_Parameter_B + (Achse * 20) + 2*i, vData , 2);
  }
}


void tServoEEPROM::Set_EEPROMMIRRROR(uint16_t Addr, uint8_t value)
{
  if (Addr < fEEpromMirrorSize)
  {
    pEEpromReadMirror[Addr] = value;
    pEEpromWriteMirror[Addr] = value;
  }
}

uint8_t tServoEEPROM::Check_EEPROMMIRRROR(uint16_t *Addr)
{
  int result = memcmp(pEEpromReadMirror, pEEpromWriteMirror, fEEpromMirrorSize);
  if (result != 0)
  {
    for (uint16_t i=0; i<fEEpromMirrorSize; i++)
      if (memcmp(pEEpromReadMirror+i, pEEpromWriteMirror+i, 1)!=0)
      {
        *Addr = i;
        return false;
      }
  }
  return !result;
}

uint32_t tServoEEPROM::MirrorSize(void)
{
  return fEEpromMirrorSize;
}


void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	for (int i=0; i<tServoEEPROM::fInstanceCounter; i++)
  {
	  if (tServoEEPROM::handlers[i]->pMyhI2C == hi2c)
 {
	  	tServoEEPROM::handlers[i]->EEPROM_Done();
	  }
 }
}


void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
  for (int i=0; i<tServoEEPROM::fInstanceCounter; i++)
  {
    if (tServoEEPROM::handlers[i]->pMyhI2C == hi2c)
 {
      tServoEEPROM::handlers[i]->EEPROMM_Error();
    }
 }
}


void tServoEEPROM::EEPROM_Done(void)
{
  if (HAL_I2C_GetState(pMyhI2C) == HAL_I2C_STATE_READY)
  {
     Set_EEPROMMIRRROR(fEPPROM_DATA_LATCH.addr, fEPPROM_DATA_LATCH.data);
  }
  else
  {
    EEPROMM_Error();
  }
  EEPROM_FIFO_STATE = EEPROM_FIFO_WS_IDLE;
}

void tServoEEPROM::EEPROMM_Error(void)
{
  EEPROM_FIFO_STATE = EEPROM_FIFO_WS_IDLE;
}

void  tServoEEPROM::EEPROM_TIMER_CALL_EVERY_MS(void)
{
  if (EEPROM_Timer) EEPROM_Timer--;
}


void tServoEEPROM::EEPROM_WRITER(void)
{
  uint16_t vAdress;
  if (pMyhI2C == NULL)
    return;

  if(__HAL_I2C_GET_FLAG(pMyhI2C, I2C_FLAG_BUSY) == SET)
  {
	  EEPROM_Timer = 20;//Damit die Writer_Done-Funktion immer eine Verz�gerung von >3ms hat
    return;
  }
  if (HAL_I2C_GetState(pMyhI2C) != HAL_I2C_STATE_READY)
	 EEPROM_Timer = 20; // Schreibverz�gerung im seriellen EEPROM 3 sind 3,6ms und zu wenig

  if ((EEPROM_Timer==0) && (!Check_EEPROMMIRRROR(&vAdress)))
  {
    fEPPROM_DATA_LATCH.addr = vAdress;
    fEPPROM_DATA_LATCH.data = Get_EEPROMMIRROR_WIRTE(vAdress);
    if (fSerialEEPROMConnect)
    {
      HAL_I2C_Mem_Write_IT(pMyhI2C, MEM_DEVICE_WRITE_ADDR, fEPPROM_DATA_LATCH.addr, _MemAddSize, &fEPPROM_DATA_LATCH.data, 1);
    }
    EEPROM_FIFO_STATE = EEPROM_FIFO_WS_DATA;
    EEPROM_Timer = 20; // Schreibverz�gerung im seriellen EEPROM 3 sind 3,6ms und zu wenig
  }
}
